#include "Component.h"
#include <Windows.h>
#include "GameObject.h"

Component::Component()
{
	m_ComponentName = "default";
	m_ComponentID = NULL;
}


Component::~Component()
{
}

string Component::ComponentName() const
{
	return m_ComponentName;
}

void Component::SetGameObject(GameObject* object)
{
	m_GameObject = object;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 2);
	cout << "Added: " << m_ComponentName << " to" << " '" << object->Name() << "' " << "gameobject." << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
}

int Component::ComponentID() const
{
	return m_ComponentID;
}
