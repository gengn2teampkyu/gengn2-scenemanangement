#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#define GLM_FORCE_RADIANS
#include <glm/gtx/transform.hpp>

#include "Component.h"
#include "TagsDefinitions.h" // Library of Tags
//! A device that enables the user to view the scene.
class Camera : public Component
{
public:
	//! A constructor
	/*!
		\param pos a glm::vec3U& that specifies the initial position of the camera
		\param fov a float the specifies the field of view of the camera
		\param aspect a float that specifies the aspect of the camera
		\param znear a float that specifies the znear of the camera
		\param zfar a float that specifies the zfar of the camera
	*/
	Camera(const glm::vec3& pos,float fov, float aspect, float znear, float zfar);
    glm::mat4 GetViewProjection() const;

	// override
	void updateComponent() override;
protected:
private:
    glm::mat4 m_prespective;
    glm::vec3 m_position;
    glm::vec3 m_forward;
    glm::vec3 m_up;

	void onDestroy() override;
};

#endif
