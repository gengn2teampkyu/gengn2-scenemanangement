#ifndef _TICK_TACK_TOE_GAME_H_
#define _TICK_TACK_TOE_GAME_H_

#include "PKYUEngine.h"

// Game Components
#include "TileGenerator.h"

class TickTackToe : public Scene
{
public:
	virtual TickTackToe* clone()
	{
		return(new TickTackToe(*this));
	}

	TickTackToe(string name) : Scene(name){}
	~TickTackToe();

	// Input Manager
	InputManager& inputManager = InputManager::getInstance();

	// Camera
	GameObject* cameraGameObject;
	Camera* camera;

	// Tile Generator
	TileGenerator* generator;

	// Player Selector
	GameObject* selector;
	ModelRenderer* selectorModelRenderer;

	// Cross
	GameObject* cross;
	ModelRenderer* crossModelRenderer;

	// Circle
	GameObject* circle;
	ModelRenderer* circleModelRenderer;

	// Music
	GameObject* ticktacktoeSoundHandler;
	AudioComponent* backgroundMusicTickTackToe;

	// Networking Constants
	const unsigned short PORT = 54000;
	const sf::IpAddress *partner;
	bool isPressed;

	// Network Set-Up
	string ipAddress;
	NetworkManager* networkManager;
	int networkOption;
	int time;
	sf::Thread* thread;
	
	enum State
	{
		STATE_SELECT,
		STATE_WAITING,
	};

	int selected = 0;
	int selectedPlayer2 = 0;
	bool hasSelected = false;

	State state = State::STATE_SELECT;

	void OnEnable() override
	{
		///// Reallocating Memory/////
		// Camera
		cameraGameObject = new GameObject("CameraObject");
		camera = new Camera(glm::vec3(0, 0, 0), 70.0f, 400.0f / 300.0f, 0.01f, 1000.0f);
		// Player Selector
		selector = new GameObject("Selector");
		selectorModelRenderer = new ModelRenderer();

		// Cross
		cross = new GameObject("Cross");
		crossModelRenderer = new ModelRenderer();

		// Circle
		circle = new GameObject("Circle");
		circleModelRenderer = new ModelRenderer();

		// Music
		ticktacktoeSoundHandler = new GameObject("ticktacktoeSoundHandler");
		backgroundMusicTickTackToe = new AudioComponent();

		// Initializing AudioComponent and Adding to a GameObject
		backgroundMusicTickTackToe->initializeSound("backgroundMusicTickTackToe", AudioComponent::SoundType::Music, "Assets/Audio/TickTackToeMusic.wav", 1000, false);
		ticktacktoeSoundHandler->addComponent(backgroundMusicTickTackToe);
		ticktacktoeSoundHandler->getComponent<AudioComponent>(SoundComponent)->playBGMusic();

		// Adds Model Renderer Component to Player Selector Object
		selector->addComponent(selectorModelRenderer);
		selector->getComponent<ModelRenderer>(ModelRendererComponent)->loadModel("Assets/Models/Selector.obj", "Assets/Textures/image.png");
		selector->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(0.05, 0.05, 0.05));
		selector->getComponent<Transform>(TransformComponent)->Rotate(glm::vec3(5.0f, 0.0f, 0.0f));
		selector->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(0.1f, 0.1f, 2.0f));

		// Adds Model Renderer Component to Cross Object
		cross->addComponent(crossModelRenderer);
		cross->getComponent<ModelRenderer>(ModelRendererComponent)->loadModel("Assets/Models/Cross.obj", "Assets/Textures/image.png");
		cross->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(0.05, 0.05, 0.05));

		// Adds Model Renderer Component to Circle Object
		circle->addComponent(circleModelRenderer);
		circle->getComponent<ModelRenderer>(ModelRendererComponent)->loadModel("Assets/Models/Circle.obj", "Assets/Textures/image.png");
		circle->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(0.05, 0.05, 0.05));

		// Load .map file to generate TickTackToe Board
		generator = new TileGenerator((char*)"Assets/Maps/ticktacktoeboardguide.map");

		// Set desired position of Camera Component
		cameraGameObject->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f)); //Sets cameraGameObject transform position
		cameraGameObject->addComponent(camera);
	}

	void Start() override
	{
		cameraGameObject->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(1.0f, 1.0f, 5.0f));

		// Adds Generator's tiles to Scene
		for each (Tile* var in generator->getTiles())
			Scene::addChild(var);

		// Adds object to Scene
		Scene::addChild(cameraGameObject);
		Scene::addChild(selector);
		Scene::addChild(cross);
		Scene::addChild(ticktacktoeSoundHandler);
		cout << "Scene Child count: " << m_childObjects.size() << endl;

		networkManager = new NetworkManager(PORT);

		// Set Up Network Options
		cout << "PKYU Engine - TickTackToe Network Game" << endl;
		cout << endl;
		
		cout << "Selection Network Option: " << endl << "1. Server" << endl << "2. Client" << endl << "Option: ";
		cin >> networkOption;
		if (!(networkOption == 1 || networkOption == 2)) return;

		// Accept Partner's IP Address
		cout << "Other Player's IP Address: ";
		cin >> ipAddress;
		partner = new sf::IpAddress(ipAddress);
		

		if (networkOption == 1) state = State::STATE_SELECT;
		else state = State::STATE_WAITING;

		// Make Thread for Networking
		thread = new sf::Thread(&NetworkManager::threadRun, networkManager);
	}

	int currentTile = 6;
	int currentTurn = 0;

	Tile* SelectTile()
	{
		return generator->getTiles()[currentTile];
	}

	glm::vec3 SearchNextNode(string move)
	{
		if (move == "UP")
		{
			if ((currentTile - 3) >= 0)
			{
				currentTile -= 3;

				return glm::vec3(generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.x,
					generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.y,
					generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.z + 1);
			}
			else
				return glm::vec3(selector->getComponent<Transform>(TransformComponent)->position.x,
				selector->getComponent<Transform>(TransformComponent)->position.y,
				selector->getComponent<Transform>(TransformComponent)->position.z
				);

		}
		else if (move == "DOWN")
		{
			if ((currentTile + 3) < 9)
			{
				currentTile += 3;

				return glm::vec3(generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.x,
					generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.y,
					generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.z + 1);
			}
			else
				return glm::vec3(selector->getComponent<Transform>(TransformComponent)->position.x,
				selector->getComponent<Transform>(TransformComponent)->position.y,
				selector->getComponent<Transform>(TransformComponent)->position.z
				);
		}
		else if (move == "LEFT")
		{
			if ((currentTile - 1) >= 0)
			{
				currentTile -= 1;

				return glm::vec3(generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.x,
					generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.y,
					generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.z + 1);
			}
			else
				return glm::vec3(selector->getComponent<Transform>(TransformComponent)->position.x,
				selector->getComponent<Transform>(TransformComponent)->position.y,
				selector->getComponent<Transform>(TransformComponent)->position.z
				);
		}
		else if (move == "RIGHT")
		{
			if ((currentTile + 1) < 9)
			{
				currentTile += 1;

				return glm::vec3(generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.x,
					generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.y,
					generator->getTiles()[currentTile]->getComponent<Transform>(TransformComponent)->position.z + 1);
			}
			else
				return glm::vec3(selector->getComponent<Transform>(TransformComponent)->position.x,
				selector->getComponent<Transform>(TransformComponent)->position.y,
				selector->getComponent<Transform>(TransformComponent)->position.z
				);
		}

		cout << "Invalid input." << endl;
		return glm::vec3(selector->getComponent<Transform>(TransformComponent)->position.x,
			selector->getComponent<Transform>(TransformComponent)->position.y,
			selector->getComponent<Transform>(TransformComponent)->position.z
			);
	}


	bool launched = false;

	void Update() override
	{
		Scene::Update();
		if (!launched)
		{
			thread->launch();
			launched = true;
		}
#pragma region GameLogic
		switch (state)
		{
			case State::STATE_SELECT:
				{
					if (inputManager.wKeyPressed() && !isPressed)
					{
						selector->getComponent<Transform>(TransformComponent)->SetPosition(SearchNextNode("UP"));
						isPressed = true;
					}

					if (inputManager.sKeyPressed() && !isPressed)
					{
						selector->getComponent<Transform>(TransformComponent)->SetPosition(SearchNextNode("DOWN"));
						isPressed = true;
					}

					if (inputManager.aKeyPressed() && !isPressed)
					{
						selector->getComponent<Transform>(TransformComponent)->SetPosition(SearchNextNode("LEFT"));
						isPressed = true;
					}

					if (inputManager.dKeyPressed() && !isPressed)
					{
						selector->getComponent<Transform>(TransformComponent)->SetPosition(SearchNextNode("RIGHT"));
						isPressed = true;
					}

					if (inputManager.spaceKeyPressed() && !isPressed && SelectTile()->GetOwner() == -1)
					{	
						SelectTile()->SetOwner(currentTurn);

						isPressed = true;
						hasSelected = true;
					}

						else if (!inputManager.spaceKeyPressed() && !inputManager.wKeyPressed() && !inputManager.sKeyPressed() && !inputManager.aKeyPressed() && !inputManager.dKeyPressed())
							isPressed = false;
					
					if (hasSelected) // CURRENT TILE
					{
						state = STATE_WAITING;
						char message[1000];
						message[0] = (char)currentTile;
						networkManager->sendMessage(*partner, message);
						cout << "Send Message: " << message << endl;

						state = State::STATE_WAITING;
					}
				} break;

			case State::STATE_WAITING:
				{
					hasSelected = false;
					if (networkManager->getMessage()[0] != '.')
					{
						cout << "Has message: " << (int)(networkManager->getMessage()[0]) << endl;

						if (networkOption == 1)
						{
							generator->getTiles()[networkManager->getMessage()[0]]->SetOwner(1);
							currentTurn = 0;
						}
						else if (networkOption == 2)
						{
							generator->getTiles()[networkManager->getMessage()[0]]->SetOwner(0);
							currentTurn = 1;
						}
						networkManager->resetNetworkMessage();
						state = State::STATE_SELECT;
					}
				} break;
		}

#pragma endregion

		if (generator->getTiles()[0]->GetOwner() != -1 && generator->getTiles()[0]->GetOwner() == generator->getTiles()[1]->GetOwner() && generator->getTiles()[1]->GetOwner() == generator->getTiles()[2]->GetOwner() ||
			generator->getTiles()[3]->GetOwner() != -1 && generator->getTiles()[3]->GetOwner() == generator->getTiles()[4]->GetOwner() && generator->getTiles()[4]->GetOwner() == generator->getTiles()[5]->GetOwner() ||
			generator->getTiles()[6]->GetOwner() != -1 && generator->getTiles()[6]->GetOwner() == generator->getTiles()[7]->GetOwner() && generator->getTiles()[7]->GetOwner() == generator->getTiles()[8]->GetOwner() ||
			generator->getTiles()[0]->GetOwner() != -1 && generator->getTiles()[0]->GetOwner() == generator->getTiles()[3]->GetOwner() && generator->getTiles()[3]->GetOwner() == generator->getTiles()[6]->GetOwner() ||
			generator->getTiles()[1]->GetOwner() != -1 && generator->getTiles()[1]->GetOwner() == generator->getTiles()[4]->GetOwner() && generator->getTiles()[4]->GetOwner() == generator->getTiles()[7]->GetOwner() ||
			generator->getTiles()[2]->GetOwner() != -1 && generator->getTiles()[2]->GetOwner() == generator->getTiles()[5]->GetOwner() && generator->getTiles()[5]->GetOwner() == generator->getTiles()[8]->GetOwner() ||
			generator->getTiles()[0]->GetOwner() != -1 && generator->getTiles()[0]->GetOwner() == generator->getTiles()[4]->GetOwner() && generator->getTiles()[4]->GetOwner() == generator->getTiles()[8]->GetOwner() ||
			generator->getTiles()[2]->GetOwner() != -1 && generator->getTiles()[2]->GetOwner() == generator->getTiles()[4]->GetOwner() && generator->getTiles()[4]->GetOwner() == generator->getTiles()[6]->GetOwner()
			)
		{
			GetManager().AccessWindow()->close();
		}

		for each (Tile* var in generator->getTiles())
		{
			if (var->GetOwner() == 0 && !var->hasDrawn)
			{
				Prefab* drawPrefab = new Prefab("DrawPrefab");

				drawPrefab->SaveToPrefab(*circle);
				drawPrefab->removeComponent(TransformComponent);
				drawPrefab->removeComponent(ModelRendererComponent);
				Transform* circleTransformComponent = new Transform();
				ModelRenderer* circleModelRendererComponent = new ModelRenderer();
				drawPrefab->addComponent(circleTransformComponent);
				drawPrefab->addComponent(circleModelRendererComponent);
				drawPrefab->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(0.05f, 0.05f, 0.05f));
				drawPrefab->getComponent<Transform>(TransformComponent)->Rotate(glm::vec3(90.0f, 0.0f, 0.0f));
				drawPrefab->getComponent<ModelRenderer>(ModelRendererComponent)->loadModel("Assets/Models/Circle.obj", "Assets/Textures/image.png");

				Scene::addChild(drawPrefab);
				drawPrefab->InstantiatePrefab(glm::vec3(var->getComponent<Transform>(TransformComponent)->position.x,
					var->getComponent<Transform>(TransformComponent)->position.y,
					var->getComponent<Transform>(TransformComponent)->position.z + 1));

				var->hasDrawn = true;
			}

			else if (var->GetOwner() == 1 && !var->hasDrawn)
			{
				Prefab* drawPrefab = new Prefab("DrawPrefab");

				drawPrefab->SaveToPrefab(*circle);
				drawPrefab->removeComponent(TransformComponent);
				drawPrefab->removeComponent(ModelRendererComponent);
				Transform* crossTransformComponent = new Transform();
				ModelRenderer* crossModelRendererComponent = new ModelRenderer();
				drawPrefab->addComponent(crossTransformComponent);
				drawPrefab->addComponent(crossModelRendererComponent);
				drawPrefab->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(0.05f, 0.05f, 0.05f));
				drawPrefab->getComponent<Transform>(TransformComponent)->Rotate(glm::vec3(90.0f, 45.0f, 0.0f));
				drawPrefab->getComponent<ModelRenderer>(ModelRendererComponent)->loadModel("Assets/Models/Cross.obj", "Assets/Textures/image.png");

				Scene::addChild(drawPrefab);
				drawPrefab->InstantiatePrefab(glm::vec3(var->getComponent<Transform>(TransformComponent)->position.x,
					var->getComponent<Transform>(TransformComponent)->position.y,
					var->getComponent<Transform>(TransformComponent)->position.z + 1));

				var->hasDrawn = true;
			}
		}

		// Changes between scenes
		if (inputManager.num1KeyPressed()) GetManager().LoadScene(0);
		if (inputManager.num2KeyPressed()) GetManager().LoadScene(1);
	}
};
#endif