#pragma once
#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <string>
#include "GL\glew.h"
#include <cassert>
#include <iostream>
#include "stb_image.h"

using namespace std;

//! A class used to load texture files.
class Texture
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Creates a new Texture.
	*/
	Texture();

	//! A constructor
	/*!
		Creates a new Texture and loads the specified file.
		\param fileName a const string& that contains the fileName of the texture that will be loaded.
	*/
	Texture(const string& fileName);

	//! A deconstructor
	/*!
		Deallocates memory of the texture during its lifetime.
	*/
	virtual ~Texture();

#pragma endregion

#pragma region Public functions for Textures

	//! Loads the specified file.
	/*!
		\param fileName a const string& that contains the fileName of the texture that will be loaded.
	*/
	void initTextureFile(const string& fileName);

	//! Binds the texture to a unit.
	/*!
		\param unit an unsigned int that contains the value of the unit.
	*/
	void bind(unsigned int unit);

#pragma endregion

private:

#pragma region Private members for Textures

	//! Contaisn the texture used (GLuint).
	GLuint m_texture;

#pragma endregion

};

#endif

