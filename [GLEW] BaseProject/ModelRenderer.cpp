#include "ModelRenderer.h"
#include "GameObject.h"


ModelRenderer::ModelRenderer()
{
	m_ComponentID = ModelRendererComponent;
	m_ComponentName = "ModelRenderer Component";
}


ModelRenderer::~ModelRenderer()
{

}

void ModelRenderer::loadModel(const string& modelFileName, const string& textureFileName)
{
	m_shader.init((char*)"res/basicShader");
	m_mesh.initModel(modelFileName);
	m_texture.initTextureFile(textureFileName);
	
}

void ModelRenderer::loadTextureFile(const string& fileName)
{
	m_shader.init((char*)"res/basicShader");
	m_texture.initTextureFile(fileName);
}

void ModelRenderer::loadModel(const string& fileName)
{
	m_shader.init((char*)"res/basicShader");
	m_mesh.initModel(fileName);
}

void ModelRenderer::updateComponent()
{
	if (m_GameObject->getParent()->hasChild("CameraObject"))
	{
		if (m_GameObject->getParent()->getChild("CameraObject")->hasComponent(CameraComponent))
		{
			m_Camera = m_GameObject->getParent()->getChild("CameraObject")->getComponent<Camera>(CameraComponent);
			renderModel(*m_Camera);
		}
	}

	// Renders Model
	if (m_GameObject->getParent()->hasComponent(ModelRendererComponent))
	{
		if (m_GameObject->getParent()->getComponent<ModelRenderer>(ModelRendererComponent)->hasCamera())
			renderModel(*m_GameObject->getParent()->getComponent<ModelRenderer>(ModelRendererComponent)->getCamera());
	}

	// Update Position 
	m_transform.GetPos()->x = m_GameObject->getComponent<Transform>(TransformComponent)->position.x;
	m_transform.GetPos()->y = m_GameObject->getComponent<Transform>(TransformComponent)->position.y;
	m_transform.GetPos()->z = m_GameObject->getComponent<Transform>(TransformComponent)->position.z;

	// Update Rotation
	m_transform.GetRot()->x = m_GameObject->getComponent<Transform>(TransformComponent)->rotation.x;
	m_transform.GetRot()->y = m_GameObject->getComponent<Transform>(TransformComponent)->rotation.y;
	m_transform.GetRot()->z = m_GameObject->getComponent<Transform>(TransformComponent)->rotation.z;

	// Update Scale
	m_transform.GetScale()->x = m_GameObject->getComponent<Transform>(TransformComponent)->scale.x;
	m_transform.GetScale()->y = m_GameObject->getComponent<Transform>(TransformComponent)->scale.y;
	m_transform.GetScale()->z = m_GameObject->getComponent<Transform>(TransformComponent)->scale.z;
}
void ModelRenderer::renderModel(const Camera& camera)
{
	m_shader.bind();
	m_shader.update(m_transform, camera);
	m_texture.bind(0);
	m_mesh.renderModel();
}

void ModelRenderer::onDestroy()
{

}
