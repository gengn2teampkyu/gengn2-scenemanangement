#include "Mesh.h"

Mesh::Mesh(Vertex* vertices,unsigned int numVertices)
{
    init(vertices,numVertices);
}

Mesh::Mesh(const string& fileName)
{
	
	initModel(fileName);
}

Mesh::Mesh()
{
}


void Mesh::init(Vertex* vertices,unsigned int numVertices)
{
    n_drawCount = numVertices;

    glGenVertexArrays(1, &m_vertexArrayObject);
    glBindVertexArray(m_vertexArrayObject);

    std::vector<glm::vec3> positions;
    
    positions.reserve(numVertices);

    for(unsigned i = 0; i < numVertices; i++){
        positions.push_back(vertices[i].pos);
    }

    glGenBuffers(NUM_BUFFERS, m_vertexArrayBuffers);

    //vertex buffer data
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * numVertices,
        &positions[0], GL_STATIC_DRAW);


    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);
}

void Mesh::initModel(const string& fileName)
{
	IndexedModel model = OBJModel(fileName).ToIndexedModel();
	n_drawCount = model.indices.size();

	glGenVertexArrays(1, &m_vertexArrayObject);
	glBindVertexArray(m_vertexArrayObject);

	glGenBuffers(NUM_BUFFERS, m_vertexArrayBuffers);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER,//what kind of data
		sizeof(model.positions[0]) * model.positions.size(),//the size of data
		&model.positions[0],//starting address
		GL_STATIC_DRAW);//draw hint

	//divides data into attributes
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexArrayBuffers[TEXCOORD_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(model.texCoords[0]) * model.positions.size(),
		&model.texCoords[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	//its not an array but a buffer that points to the elements of another
	//array.
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vertexArrayBuffers[INDEX_VB]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, model.indices.size()*sizeof(model.indices[0]),
		&model.indices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);
}


Mesh::~Mesh()
{
    glDeleteVertexArrays(1, &m_vertexArrayObject);
}

void Mesh::draw()
{
    glBindVertexArray(m_vertexArrayObject);
	glDrawArrays(GL_TRIANGLES, 0, n_drawCount);
	//glDrawElements(GL_TRIANGLES, n_drawCount, GL_UNSIGNED_INT,
		//0);//where to start
    glBindVertexArray(0);
}

void Mesh::renderModel()
{
	glBindVertexArray(m_vertexArrayObject);
	glDrawElements(GL_TRIANGLES, n_drawCount, GL_UNSIGNED_INT,
	0);//where to start
	glBindVertexArray(0);
}
