#ifndef _SCENE_2_H_
#define _SCENE_2_H_

#include "PKYUEngine.h"
class ExplorationScene : public Scene
{
public:
	virtual ExplorationScene* clone()
	{
		return(new ExplorationScene(*this));
	}

	ExplorationScene(string n) : Scene(n){}
	~ExplorationScene();

	// Input Manager
	InputManager& inputManager = InputManager::getInstance();
	// Camera
	Camera* camera;

	GameObject* sampleGameObject; // Creation of GameObject
	GameObject* cameraGameObject;
	//CubeRenderer* newmesh = new CubeRenderer();

	//////// Model Renderer ///////////
	ModelRenderer* modelRenderer;

	void OnEnable() override
	{
		sampleGameObject = new GameObject("SampleObject"); // Creation of GameObject
		cameraGameObject = new GameObject("CameraObject");
		modelRenderer = new ModelRenderer();
		camera = new Camera(glm::vec3(0, 0, 0), 70.0f, 400.0f / 300.0f, 0.01f, 1000.0f);

		BackgroundColor = Vector4(0, 0.6, 0, 0);

		// TODO: Store a default variable for pos, rot, and scale
		sampleGameObject->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
		sampleGameObject->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(1.0f, 1.0f, 1.0f));
		cameraGameObject->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	}

	void Start() override
	{
		cout << GameObject::Name() << " initiated." << endl;

		//sampleGameObject->addComponent(newmesh);
		sampleGameObject->addComponent(modelRenderer);
		cameraGameObject->addComponent(camera);
		
		Scene::addChild(sampleGameObject);
		Scene::addChild(cameraGameObject);
		//cout << "Game children count: " << m_childObjects.size() << endl;

		//sampleGameObject->getComponent<CubeRenderer>(CubeRendererComponent)->initializeMesh(glm::vec3(0.0f, 0.1f, 0.5f), 0.050f);

		///// Model Renderer /////
		sampleGameObject->getComponent<ModelRenderer>(ModelRendererComponent)->loadModel("Assets/Models/monkey3.obj", "Assets/Textures/image.png");

		// Sound //
		AudioComponent* samplesound = new AudioComponent();
		samplesound->initializeSound("sample", AudioComponent::Music, "Assets/Audio/Exploration.wav", 1000, false);
		samplesound->playBGMusic();

		// Add SoundComponent to SampleGameObject
		sampleGameObject->addComponent(samplesound);
		//Scene::addComponent(samplesound);
		
	}


	void Update() override
	{
		Scene::Update();
		//cameraGameObject->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(0.0f, 0.0f, 0.01f));
		sampleGameObject->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(0.01f, 0.0f, 0.0f));
		//cout << "MONKEY ADDRESS: " << sampleGameObject << endl;


		if (inputManager.num1KeyPressed())
			GetManager().LoadScene(0);
		if (inputManager.num2KeyPressed())
			GetManager().LoadScene(1);
	}
};
#endif