#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <iostream>
#include <fstream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "ModelTransform.h"
#include "Camera.h"

//! A class used to load shader files
class Shader
{
public:
	//! A constructor
	/*!
		Creates a new Shader that loads the specified file.
		\param fileName a const std::string& that contains the file name of shader that will be loaded.
	*/
	Shader(const std::string& fileName);
	
	//! A constructor
	/*!
		Creates a new Shader.
	*/
	Shader();
	
	//! Initializes the shader that will be used.
	/*!
		\param fileName a const std::string& that contains the file name of shader that will be loaded.
	*/
	void init(const std::string& fileName);

	//! A deconstructor
	/*!
		Deallocates memory of the shader during its lifetime.
	*/
    virtual ~Shader();

	//! Binds the shader.
	void bind();
	//! Updates the shader based on the model's transform.
	/*!
		\param transform a ModelTransform& that contains the transform of the model.
	*/
    void update(ModelTransform& transform);
	//! Updates the shader based on the model's transform and camera.
	/*!
		\param transform a ModelTransform& that contains the transform of the model.
		\param cam a const Camera& that contains the Scene's camera.
	*/
	void update(ModelTransform& transform, const Camera& cam);
	//! Updates the shader based on the model's transform and camera while updating the shader's color.
	/*!
		\param transform a ModelTransform& that contains the transform of the model.
		\param cam a const Camera& that contains the Scene's camera.
		\param color a const glm::vec3 that contains the value of the desired color.
	*/
	void update(ModelTransform& transform, const Camera& cam, const glm::vec3 color);
protected:
private:
	//! Contains the number of Shaders
	static const unsigned int NUM_SHADERS = 2;
	//static const unsigned int NUM_UNIFORMS = 3;
    enum{TRANSFORM_U,
        COLOR_U,
        NUM_UNIFORMS};

	//! Returns the specified shader
	/*!
		\param fileName a const std::string& that contains the name of the file of the shader.
	*/
	std::string LoadShader(const std::string& fileName);

	//! Checks for error
	/*!
		\param shader a GLuint that contains the target shader.
		\param flag a GLuint that contains the flag
		\param isProgram a bool that specifies if target is program.
		\param errorMessage a const std::string& that contains the message.
	*/
	void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage);
	
	//! Creates a shader
	/*!
		\param text a const std::string that contains the shader source.
		\param type a GLenum that contains the type.
	*/
	GLuint CreateShader(const std::string& text, GLenum type);

	//! Contains the program
	GLuint m_program;
	//! Contains the shaders
	GLuint m_shaders[NUM_SHADERS];
	//! Contains uniforms
	GLuint m_uniforms[NUM_UNIFORMS];
};

#endif
