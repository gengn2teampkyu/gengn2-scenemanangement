#include "InputManager.h"


InputManager::InputManager()
{
}


InputManager::~InputManager()
{
}


// Letter Inputs
bool InputManager::aKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::A);
}

bool InputManager::bKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::B);
}

bool InputManager::cKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::C);
}

bool InputManager::dKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::D);
}

bool InputManager::eKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::E);
}

bool InputManager::fKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::F);
}

bool InputManager::gKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::G);
}

bool InputManager::hKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::H);
}

bool InputManager::iKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::I);
}

bool InputManager::jKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::J);
}

bool InputManager::kKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::K);
}

bool InputManager::lKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::L);
}

bool InputManager::mKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::M);
}

bool InputManager::nKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::N);
}

bool InputManager::oKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::O);
}

bool InputManager::pKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::P);
}

bool InputManager::qKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Q);
}

bool InputManager::rKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::R);
}

bool InputManager::sKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::S);
}

bool InputManager::tKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::T);
}

bool InputManager::uKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::U);
}

bool InputManager::vKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::V);
}

bool InputManager::wKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::W);
}

bool InputManager::xKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::X);
}

bool InputManager::yKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Y);
}

bool InputManager::zKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Z);
}

// Numerical Inputs
bool InputManager::num1KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num1);
}

bool InputManager::num2KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num2);
}

bool InputManager::num3KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num3);
}

bool InputManager::num4KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num4);
}

bool InputManager::num5KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num5);
}

bool InputManager::num6KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num6);
}

bool InputManager::num7KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num7);
}

bool InputManager::num8KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num8);
}

bool InputManager::num9KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num9);
}

bool InputManager::num0KeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Num0);
}

// Special Key Inputs
bool InputManager::escapeKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Escape);
}

bool InputManager::spaceKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Space);
}

bool InputManager::enterKeyPressed()
{
	return sf::Keyboard::isKeyPressed(sf::Keyboard::Return);
}

