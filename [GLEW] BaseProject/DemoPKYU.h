#ifndef _DEMO_PKYU_H_
#define _DEMO_PKYU_H_

#include "PKYUEngine.h"

class DemoPKYU : public Scene
{
public:
	virtual DemoPKYU* clone()
	{
		return(new DemoPKYU(*this));
	}
	DemoPKYU(string name) : Scene(name){}
	~DemoPKYU();

	// Input Manager
	InputManager& inputManager = InputManager::getInstance();

	// Camera
	GameObject* cameraGameObject;
	Camera* cameraComponent;

	// Demo Object and Components
	GameObject* sampleGameObject;
	Transform* transformComponent;
	ModelRenderer* modelRendererComponent;
	AudioComponent* soundComponent;

	// Checker
	bool pressed;

	void OnEnable() override
	{
		// Reallocating Memory - Camera and Components
		cameraGameObject = new GameObject("CameraObject");
		cameraComponent = new Camera(glm::vec3(0, 0, 0), 70.0f, 400.0f / 300.0f, 0.1f, 1000.0f);

		// Adding Components to Camera
		cameraGameObject->addComponent(cameraComponent);

		// Reallocating Memory - GameObject and Components
		sampleGameObject = new GameObject("Sample Game Object");
		transformComponent = new Transform();
		modelRendererComponent = new ModelRenderer();
		soundComponent = new AudioComponent();
	}

	void Start() override
	{
		// Adds object to Scene
		Scene::addChild(cameraGameObject);
	}

	void Update() override
	{
		Scene::Update();
	
		// Demo Flow
		/*
			1 - Press Enter to Make Game Object a child of the Scene
			2 - Press Q to add the Model Renderer Component to Game Object
			3 - Press W to tranlate the Transform Component of the Game Object
			5 - Press R to add Sound Component to Game Object
			6 - Press T to Initialize Sound First BGM
			7 - Press Y to Initialize Sound Second BGM
			8 - Press U to stop music
			9.a - Press 1 to go to the Sliding Game
			9.b - Press 2 to go to the Network Tic-Tac-Toe Game
		*/
		if (inputManager.enterKeyPressed())
		{
			Scene::addChild(sampleGameObject);
		}
		else if (inputManager.qKeyPressed())
		{
			sampleGameObject->addComponent(modelRendererComponent);
			sampleGameObject->getComponent<ModelRenderer>(ModelRendererComponent)->loadModel("Assets/Models/monkey3.obj", "Assets/Textures/image.png");
		}
		else if (inputManager.wKeyPressed())
		{
			sampleGameObject->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(0.0f, 0.0f, -0.5f));
		}
		else if (inputManager.spaceKeyPressed())
		{
			sampleGameObject->getComponent<Transform>(TransformComponent)->Rotate(glm::vec3(0.0f, 0.25f, 0.0f));
		}
		else if (inputManager.rKeyPressed())
		{
			sampleGameObject->addComponent(soundComponent);
		}
		else if (inputManager.tKeyPressed())
		{
			sampleGameObject->getComponent<AudioComponent>(SoundComponent)->initializeSound("BackgroundMusic", AudioComponent::SoundType::Music, "Assets/Audio/Sample1.wav", 1000, false);
			sampleGameObject->getComponent<AudioComponent>(SoundComponent)->playBGMusic();
		}
		else if (inputManager.yKeyPressed())
		{
			sampleGameObject->getComponent<AudioComponent>(SoundComponent)->initializeSound("BackgroundMusic", AudioComponent::SoundType::Music, "Assets/Audio/Sample2.wav", 1000, false);
			sampleGameObject->getComponent<AudioComponent>(SoundComponent)->playBGMusic();
		}
		else if (inputManager.uKeyPressed())
		{
			sampleGameObject->getComponent<AudioComponent>(SoundComponent)->stopBGMPlayback();
		}
		else if (inputManager.iKeyPressed())
		{
			sampleGameObject->getComponent<AudioComponent>(SoundComponent)->playBGMusic();
		}
		else if (inputManager.num1KeyPressed())
		{
			GetManager().LoadScene(0);
		}
		else if (inputManager.num2KeyPressed())
		{
			GetManager().LoadScene(1);
		}

	}

};

#endif