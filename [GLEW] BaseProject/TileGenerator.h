#pragma once
#ifndef _TILE_GENERATOR_H_
#define _TILE_GENERATOR_H_

#include <iostream>
#include <vector>
#include <fstream>
#include "TagsDefinitions.h"
#include "Tile.h"

using namespace std;

//! Generates tiles based on a .map file
class TileGenerator
{
public:
	//! A constructor
	TileGenerator();
	//! A constructor
	/*!
		\param fileName a char that contains the file name of the map.
	*/
	TileGenerator(char fileName[]);
	//! A deconstructor
	~TileGenerator();

	//! Returns the tileContent of the tile generator.
	vector<vector<char>> tileContent;

	//! Returns vector of tiles.
	vector<Tile*> getTiles() { return m_Tiles; }
	
	//! Returns the initial player position. (Used for Sliding Game)
	glm::vec3 playerPosition;
private:
	//! Initializes tile map.
	/*!
		\param fileName a char that contains the file name of the map.
	*/
	void initializeTiles(char fileName[]);

	//! Contains the vector of tile.
	vector<Tile*> m_Tiles;

	//! Contains the position of the Generator.
	glm::vec3 m_Position;
};

#endif

