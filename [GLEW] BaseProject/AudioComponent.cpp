#include "AudioComponent.h"


AudioComponent::AudioComponent()
{
	m_SoundName = "default";
	m_ComponentID = SoundComponent;
}


AudioComponent::~AudioComponent()
{
	m_Music->stop();
	m_Sound.stop();
}

void AudioComponent::initializeSound(string name, AudioComponent::SoundType type, const string& fileName, float volume, bool autoPlay)
{
	m_SoundName = name;
	m_type = type;
	m_Music = unique_ptr<sf::Music>(new sf::Music());
	m_SoundBuffer = unique_ptr<sf::SoundBuffer>(new sf::SoundBuffer());
	m_autoPlay = autoPlay;
	if (type == AudioComponent::Music)
	{
		if (!m_Music->openFromFile(fileName))
			cout << "No Music File Detected" << endl;
	}
	else
	{
		if (!m_SoundBuffer->loadFromFile(fileName))
			cout << "No File Detected" << endl;
	}

	m_Music->setVolume(volume);
}

void AudioComponent::playBGMusic()
{
	m_Music->play();
}

void AudioComponent::setBGVolume(float volume)
{
	m_Music->setVolume(volume);
}

void AudioComponent::stopBGMPlayback()
{
	m_Music->stop();
}

void AudioComponent::playSFX()
{
	m_Sound.setBuffer(*m_SoundBuffer);
	m_Sound.play();
}

void AudioComponent::setSFXVolume(float volume)
{
	m_Sound.setVolume(volume);
}

void AudioComponent::updateComponent()
{
	if (m_autoPlay) m_Music->play();
}

void AudioComponent::onDestroy()
{
	cout << "Destroy Audio!" << endl;
	stopBGMPlayback();
    m_Music = nullptr;
	m_SoundBuffer = nullptr;
	m_Sound.stop();
}
