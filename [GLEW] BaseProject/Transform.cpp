#include "Transform.h"
#include "GameObject.h"

Transform::Transform()
{
	m_ComponentName = "Transform Component";
	m_ComponentID = TransformComponent;

	// Load Default Modules
	loadDefaultModules();
}

Transform::~Transform()
{
	loadDefaultModules();
}

void Transform::updateComponent()
{
	if (m_GameObject->getParent()->hasComponent(TransformComponent))
	{
		parentPos = m_GameObject->getParent()->getComponent<Transform>(TransformComponent)->position;
		parentRot = m_GameObject->getParent()->getComponent<Transform>(TransformComponent)->rotation;
		parentScale = m_GameObject->getParent()->getComponent<Transform>(TransformComponent)->scale;
	}

	position.x = curPos.x + parentPos.x;
	position.y = curPos.y + parentPos.y;
	position.z = curPos.z + parentPos.z;

	rotation.x = curRot.x + parentRot.x;
	rotation.y = curRot.y + parentRot.y;
	rotation.z = curRot.z + parentRot.z;

	//scale.x = curScale.x + parentScale.x;
	//scale.y = curScale.y + parentScale.y;
	//scale.z = curScale.z + parentScale.z;
}

void Transform::Translate(glm::vec3 direction)
{
	curPos.x += direction.x;
	curPos.y += direction.y;
	curPos.z += direction.z;
}

void Transform::Rotate(glm::vec3 rotationVal)
{
	curRot.x += rotationVal.x;
	curRot.y += rotationVal.y;
	curRot.z += rotationVal.z;
}

void Transform::Scale(glm::vec3 scaleVal)
{
	curScale.x += scaleVal.x;
	curScale.y += scaleVal.y;
	curScale.z += scaleVal.z;
}

void Transform::SetPosition(glm::vec3 pos)
{
	position.x = pos.x;
	position.y = pos.y;
	position.z = pos.z;

	//TEST:
	curPos.x = pos.x;
	curPos.y = pos.y;
	curPos.z = pos.z;
}

void Transform::SetRotation(glm::vec3 rot)
{
	rotation.x = rot.x;
	rotation.y = rot.y;
	rotation.z = rot.z;
}

void Transform::SetScale(glm::vec3 scaleVal)
{
	scale.x = scaleVal.x;
	scale.y = scaleVal.y;
	scale.z = scaleVal.z;
}

void Transform::loadDefaultModules()
{
	x = 0;
	y = 0;
	z = 0;
	xRot = 0;
	yRot = 0;
	zRot = 0;
	scale.x = 1;
	scale.y = 1;
	scale.z = 1;
	//NOTE: Other Scale
	xScale = 1;
	yScale = 1;
	zScale = 1;
}

void Transform::onDestroy()
{
	loadDefaultModules();
}