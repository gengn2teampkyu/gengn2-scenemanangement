#pragma once
#ifndef _TEXT_RENDERER_H_
#define _TEXT_RENDERER_H_

#include "Component.h"
#include <string>
#include <glm\glm.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <stdio.h>
#include <vector>

using namespace std;

#include "TagsDefinitions.h"

//! A component that renders Texts
class TextRenderer : public Component
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Creates a new Text Renderer that renders texts.
	*/
	TextRenderer();

	//! A deconstructor
	/*!
		Deallocates memory of the Text Renderer during its lifetime.
	*/
	~TextRenderer();

#pragma endregion

#pragma region Public functions for TextRenderers

	//! Updates the component
	void updateComponent() override;

	//! Initializes the text to be displayed
	/*!
		\param fontDirectory a const string& that contains the file name of the font style.
		\param size a float that specifies the font size of the text.
		\param pos a glm::vec2 that specifies the position of the text.
		\param color an sf::Color that specifies the color of the text.
		\param textToDisplay a const string& that contains the text that will be displayed.
	*/
	void initializeText(const string& fontDirectory, float size, glm::vec2 pos, sf::Color color, const string& textToDisplay);
	//! Sets the font style
	/*!
		\param fontDirectory a const string& that contains the file name of the font style.
	*/
	void setFont(const string& fontDirectory);
	//! Sets the font size
	/*!
		\param size a float that specifies the font size of the text.
	*/
	void setFontSize(float size);
	//! Sets the text to display
	/*!
		\param textToDisplay a const string& that contains the text that will be displayed.
	*/
	void setString(const string& textToDisplay);
	//! Sets the text position
	/*!
		\param pos a glm::vec2 that specifies the position of the text.
	*/
	void setPosition(glm::vec2 pos);
	//! Sets the font color
	/*!
		\param color an sf::Color that specifies the color of the text.
	*/
	void setColor(sf::Color color);

	//! Renders text
	/*!
		\param window an sf::RenderWindow& that contains the window where the text will be rendered.
	*/
	void renderText(sf::RenderWindow& window);

#pragma endregion

#pragma region Public members for TextRenderers

	//! Contains the Font used by the TextRenderer.
	sf::Font m_font;

	//! Contains the Text to be rendered by the TextRenderer.
	sf::Text m_text;

#pragma endregion

private:
	
};

#endif

