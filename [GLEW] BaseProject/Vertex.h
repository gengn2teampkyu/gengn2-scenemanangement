#pragma once
#ifndef _VERTEX_H_
#define _VERTEX_H_

#include <glm\glm.hpp>

//! A class that creates vertex
class Vertex
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Creates a Vertex with a position value.
		\param posVal a const glm::vec3 that contains the position of the vertex.
	*/
	Vertex(const glm::vec3 posVal);

	//! A constructor
	/*!
		Creates a Vertex with a position value and a coordinate.
		\param posVal a const glm::vec3 that contains the position of the vertex.
		\param coord a const glm::vec2 that contains the texture coordinate of the vertex.
	*/
	Vertex(const glm::vec3 posVal, const glm::vec2 coord); // overload
	
	//! A deconstructor
	/*!
		Deallocates memory of the Vertex during its lifetime.
	*/
	~Vertex();

#pragma endregion

#pragma region Public members for Vertices

	//! Position of the vertex.
	glm::vec3 pos;
	//! Texture Coordinates of the vertex.
	glm::vec2 texCoord;
	
#pragma endregion

};

#endif

