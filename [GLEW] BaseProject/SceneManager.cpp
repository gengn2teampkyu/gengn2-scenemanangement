#include "SceneManager.h"

////// Modifiables //////
const int WIDTH  = 600;
const int HEIGHT = 800;
const string TITLE = "PKYU Engine V0.0.31 - Commit #: 739a0cd76fa73842a7ca2e22d165934ca202bf12";
////// End of Modifiables //////

SceneManager::SceneManager()
{
}
SceneManager::~SceneManager()
{
}

bool SceneManager::Init()
{
	if (!AccessWindow()->isOpen())
	{
		AccessWindow()->create(sf::VideoMode(HEIGHT, WIDTH), Name(), sf::Style::Default, sf::ContextSettings(32));
		AccessWindow()->setPosition(sf::Vector2i(0, 0));
		AccessWindow()->setVerticalSyncEnabled(true);

		GLenum status = glewInit();
		if (status != GLEW_OK)
			exit(0);

		glEnable(GL_TEXTURE_2D); 
		glEnable(GL_DEPTH_TEST);

		//resizeWindow(AccessWindow()->getSize().x, AccessWindow()->getSize().y);
	}

	if (!AccessWindow()->isOpen())
		return false;

	return true;
}

void SceneManager::Event()
{
	sf::Event event;
	while (AccessWindow()->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			AccessWindow()->close();
		else if (event.type == sf::Event::Resized)
			glViewport(0, 0, event.size.width, event.size.height);
		//resizeWindow(event.size.width, event.size.height);
	}
}

void SceneManager::Render()
{
	AccessWindow()->display();
}

void SceneManager::Update()
{
	m_currentScene->Update();
}

void SceneManager::MainLoop()
{
	m_currentScene->OnEnable();
	m_currentScene->Start();
	cout << "Scene number: " << sceneIndex.size() << endl;

	while (AccessWindow()->isOpen())
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Event();
		Update();
		Render();
	}
}

void SceneManager::AddToSceneList(Scene* s)
{
	sceneIndex.push_back(s);
}

void SceneManager::StartEngine()
{
	setName(TITLE);

	if (!Init())
		throw "Failed to initialize PKYU Engine";

	MainLoop();
}