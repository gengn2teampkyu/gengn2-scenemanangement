#pragma once
#ifndef _INPUT_MANAGER_H_
#define _INPUT_MANAGER_H_

#include "SFML\Window.hpp"

//! A class used for input.
class InputManager
{
public:
	//! A constructor
	InputManager();
	//! A deconstructor
	~InputManager();

	
	// Letter Inputs
	//! Returns if the A key is pressed.
	bool aKeyPressed();
	//! Returns if the B key is pressed.
	bool bKeyPressed();
	//! Returns if the C key is pressed.
	bool cKeyPressed();
	//! Returns if the D key is pressed.
	bool dKeyPressed();
	//! Returns if the E key is pressed.
	bool eKeyPressed();
	//! Returns if the F key is pressed.
	bool fKeyPressed();
	//! Returns if the G key is pressed.
	bool gKeyPressed();
	//! Returns if the H key is pressed.
	bool hKeyPressed();
	//! Returns if the I key is pressed.
	bool iKeyPressed();
	//! Returns if the J key is pressed.
	bool jKeyPressed();
	//! Returns if the K key is pressed.
	bool kKeyPressed();
	//! Returns if the L key is pressed.
	bool lKeyPressed();
	//! Returns if the M key is pressed.
	bool mKeyPressed();
	//! Returns if the N key is pressed.
	bool nKeyPressed();
	//! Returns if the O key is pressed.
	bool oKeyPressed();
	//! Returns if the P key is pressed.
	bool pKeyPressed();
	//! Returns if the Q key is pressed.
	bool qKeyPressed();
	//! Returns if the R key is pressed.
	bool rKeyPressed();
	//! Returns if the S key is pressed.
	bool sKeyPressed();
	//! Returns if the T key is pressed.
	bool tKeyPressed();
	//! Returns if the U key is pressed.
	bool uKeyPressed();
	//! Returns if the V key is pressed.
	bool vKeyPressed();
	//! Returns if the W key is pressed.
	bool wKeyPressed();
	//! Returns if the X key is pressed.
	bool xKeyPressed();
	//! Returns if the Y key is pressed.
	bool yKeyPressed();
	//! Returns if the Z key is pressed.
	bool zKeyPressed();
	// End of Letter Inputs

	// Numerical Inputs
	//! Returns if the NUM 1 key is pressed.
	bool num1KeyPressed();
	//! Returns if the NUM 2 key is pressed.
	bool num2KeyPressed();
	//! Returns if the NUM 3 key is pressed.
	bool num3KeyPressed();
	//! Returns if the NUM 4 key is pressed.
	bool num4KeyPressed();
	//! Returns if the NUM 5 key is pressed.
	bool num5KeyPressed();
	//! Returns if the NUM 6 key is pressed.
	bool num6KeyPressed();
	//! Returns if the NUM 7 key is pressed.
	bool num7KeyPressed();
	//! Returns if the NUM 8 key is pressed.
	bool num8KeyPressed();
	//! Returns if the NUM 9 key is pressed.
	bool num9KeyPressed();
	//! Returns if the NUM 0 key is pressed.
	bool num0KeyPressed();
	// End of Numerical Inputs

	// Special Key Inputs
	//! Returns if the ESCAPE key is pressed.
	bool escapeKeyPressed();
	//! Returns if the SPACE key is pressed.
	bool spaceKeyPressed();
	//! Returns if the ENTER/RETURN key is pressed.
	bool enterKeyPressed();
	// End of Special Key Inputs

	// Singleton 
	//! Returns an instance of the InputManager (Singleton)
	static InputManager& getInstance()
	{
		static InputManager instance;
		return instance;
	}
};

#endif

