#ifndef _SLIDING_GAME_H_
#define _SLIDING_GAME_H_

#include "PKYUEngine.h"

// Game Components
#include "TileGenerator.h"
#include "Player.h"

class SlidingGame : public Scene
{
public:
	virtual SlidingGame* clone()
	{
		return(new SlidingGame(*this));
	}
	SlidingGame(string name) : Scene(name){}
	~SlidingGame();


	// Input Manager
	InputManager& inputManager = InputManager::getInstance();

	// Camera
	GameObject* cameraGameObject;
	Camera* cameraComponent;

	// Tile Generator
	TileGenerator* tileGenerator;

	// Player GameObject
	Player* player;
	ModelRenderer* modelRendererComponent;
	Collider* colliderComponent;

	// Music
	GameObject* soundHandler;
	AudioComponent* backgroundMusic;

	void OnEnable() override
	{
		/*if (!font.loadFromFile(sf::String("Assets/Fonts/ubuntu.ttf"))) return;
		myText.setFont(font);
		myText.setColor(sf::Color::White);
		myText.setString("Hello Text");
		myText.setPosition(10, 10);
		myText.setCharacterSize(34);*/

		// Reallocating Memory - Camera and Components
		cameraGameObject = new GameObject("CameraObject");
		cameraComponent = new Camera(glm::vec3(0, 0, 0), 70.0f, 400.0f / 300.0f, 0.1f, 1000.0f);

		// Reallocating Memory - Player and Components
		player = new Player();
		modelRendererComponent = new ModelRenderer();
		colliderComponent = new Collider(0.25f);

		// Reallocating Memory - Sound Handler and Components
		soundHandler = new GameObject("SoundHandler");
		backgroundMusic = new AudioComponent();

		// Reallocating Memory - Tile Generator
		tileGenerator = new TileGenerator((char*)"Assets/Maps/boardguide.map");

		// Adding Components to Camera
		cameraGameObject->addComponent(cameraComponent);

		// Adding Components to Player
		player->addComponent(modelRendererComponent);
		player->addComponent(colliderComponent);

		// Adding Components to AudioComponent
		soundHandler->addComponent(backgroundMusic);
	}

	void Start() override
	{
		// Setting Camera Position
		cameraGameObject->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(2.0f, 5.0f, 15.0f));

		// Setting Background Music
		backgroundMusic->initializeSound("BackgroundMusic", AudioComponent::SoundType::Music, "Assets/Audio/SlidingGameMusic2.wav", 1000, false);
		soundHandler->getComponent<AudioComponent>(SoundComponent)->playBGMusic();

		// Setting Player ModelRenderer
		player->getComponent<ModelRenderer>(ModelRendererComponent)->loadModel("Assets/Models/monkey3.obj", "Assets/Textures/image.png");
		player->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(0.75f, 0.75f, 0.75f));
		// Adding Tiles to the Scene
		for each (Tile* tile in tileGenerator->getTiles())
			Scene::addChild(tile);

		// Adding GameObjects to the Scene
		Scene::addChild(player);
		Scene::addChild(cameraGameObject);
		Scene::addChild(soundHandler);
	}

	void Update() override
	{
		Scene::Update();

		// SAMPLE TEXT
		//GetManager().AccessWindow()->pushGLStates();
		//GetManager().AccessWindow()->draw(myText);
		//GetManager().AccessWindow()->popGLStates();

		for (int i = 0; i < tileGenerator->getTiles().size(); i++)
			tileGenerator->getTiles()[i]->getComponent<Collider>(ColliderComponent)->CheckCollision();

		if (inputManager.aKeyPressed())			player->MoveHorizontal(-0.1f);
		else if (inputManager.dKeyPressed())	player->MoveHorizontal(0.1f);
		else if (inputManager.wKeyPressed())	player->MoveVertical(0.1f);
		else if (inputManager.sKeyPressed())	player->MoveVertical(-0.1f);
		else player->Stop();

		if (inputManager.spaceKeyPressed()) player->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));

		if (player->getComponent<Collider>(ColliderComponent)->CheckCollision(FINALGOAL_TAG))
		{
			for each (Tile* tile in tileGenerator->getTiles())
			{
				if (tile->getTag() == PLAYERINITPOS_TAG)
				{
					player->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(tile->getComponent<Transform>(TransformComponent)->position.x,
						tile->getComponent<Transform>(TransformComponent)->position.y,
						tile->getComponent<Transform>(TransformComponent)->position.z));
				}
			}
		}

		// Changes between scenes
		if (inputManager.num1KeyPressed()) GetManager().LoadScene(0);
		if (inputManager.num2KeyPressed()) GetManager().LoadScene(1);
	}
};

#endif