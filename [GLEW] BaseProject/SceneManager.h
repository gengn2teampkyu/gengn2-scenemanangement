#pragma once
#ifndef _SCENEMANAGER_H_
#define _SCENEMANAGER_H_

#include "GameObject.h"
#include "Layer.h"
#include <vector>
#include <GL\glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include "Scene.h"
#include <string>

using namespace std;

//! A game object that handles the management of scenes
class SceneManager : public GameObject
{
private:

#pragma region Private functions of SceneManagers

	//! Is the SceneManager initialized?
	bool Init();

	//! Starts Current Scene.
	void Start(){};

	//! Updates Current Scene.
	void Update();

	//! Renders Current Scene.
	void Render();

	//! Flow of the SceneManager.
	void MainLoop();

	//! Runs the event for SceneManagers.
	void Event();
	
#pragma endregion

#pragma region Private members of Scene Managers

	//! Contains the Current Scene loaded.
	Scene* m_currentScene;

	//! Contains the RenderWindow used by the SceneManager.
	sf::RenderWindow window;

	//! Contains a vector of Scenes.
	vector<Scene*> sceneIndex;

#pragma endregion

public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Creates a new SceneManager.
	*/
	SceneManager();
	//! A deconstructor
	/*!
		Deallocates memory used by the SceneManager during its lifetime.
	*/
	~SceneManager();

	//! Returns the specified Scene
	/*!
		\param index an int the contains the index number of the Scene.
	*/

#pragma endregion

#pragma region Getters for SceneManagers

	//! Returns specified Scene
	/*!
		\param index an int that cointain the target index number of a Scene.
	*/
	Scene &GetScene(int index)
	{
		return *sceneIndex[index];
	}

	//! Returns the RenderWindow used by the SceneManager.
	sf::RenderWindow* AccessWindow()
	{
		return &window;
	}

#pragma endregion

#pragma region Public functions for SceneManagers

	//! Starts the Scene
	void StartEngine();

	//! Loads the specified Scene
	/*!
		\param newScene a Scene* that contains the Scene to be loaded.
	*/
	void LoadScene(int index)
	{
		if (m_currentScene != NULL)
		{
			m_currentScene->EndScene();
		}

		system("cls");
		m_currentScene = sceneIndex[index]->clone();
		m_currentScene->timesLoaded++;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
		cout << "Times scene was loaded: " << m_currentScene->timesLoaded << endl;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);

		m_currentScene->SetManager(this);

		StartEngine();
	}

	//! Adds a Scene to the list of Scenes
	/*!
		\param s a Scene* that will be added to the list of Scenes.
	*/
	void AddToSceneList(Scene* s);

#pragma endregion

#pragma region Singleton

	// Singleton
	//! Returns an instance of the SceneManager (Singleton)
	static SceneManager& getInstance()
	{
		static SceneManager instance;
		return instance;
	}

#pragma endregion

};

#endif