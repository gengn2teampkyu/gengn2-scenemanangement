#ifndef MESH_H
#define MESH_H

#include <vector>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include "Vertex.h"
#include "objLoader.h"
#include <string>

using namespace std;

//class Vertex
//{
//    public:
//        Vertex(const glm::vec3 posVal)
//		{
//            pos = posVal;
//        }
//    //private:
//        glm::vec3 pos;
//};

//! A class used to load mesh files
class Mesh
{
public:    
	//! A constructor
	/*!
		Creates a new GLEW Mesh.
		\param vertices a Vertex* that contains the vertex that will be initialized.
		\param numVertices an unsigned int that contains the number of vertices.
	*/
    Mesh(Vertex* vertices,unsigned int numVertices);

	// Model Loading
	//! A constructor
	/*!
		Creates a new Mesh that loads the specified file.
		\param fileName	a const string& that contains the file name of the model that will be loaded.
	*/
	Mesh(const string& fileName);

	//! A constructor
	/*!
		Creates a new Mesh.
	*/
	Mesh();

	//! Initializes the mesh that will be rendered.
	/*!
		\param vertices a Vertex* that contains the vertex that will be initialized.
		\param numVertices an unsigned int that contains the number of vertices.
	*/
    void init(Vertex* vertices,unsigned int numVertices);

	// Model Loading
	//! Initializes the model that will be rendered.
	/*!
		Loads the specified file.
		\param fileName	a const string& that contains the file name of the model that will be loaded.
	*/
	void initModel(const string& fileName);

	//! A deconstructor
	/*!
		Deallocates memory of the mesh during its lifetime.
	*/
    ~Mesh();

	//! Draws the mesh.
    void draw();
	//! Renders the model.
	void renderModel();
private:
    //static const unsigned int NUM_BUFFERS = 4;
	//enum{ POSITION_VB, NUM_BUFFERS, TEXCOORD_VB, INDEX_VB };
	enum{ POSITION_VB, TEXCOORD_VB, INDEX_VB, NUM_BUFFERS };

    GLuint m_vertexArrayObject;
    GLuint m_vertexArrayBuffers[NUM_BUFFERS];
    unsigned int n_drawCount;
};

#endif
