#pragma once
#ifndef _TILE_H_
#define _TILE_H_

#include "GameObject.h"

//! A GameObject that generates a tile
class Tile : public GameObject 
{
public:
	//! A constructor
	/*!
		\param size a float that specifies the size of the tile.
		\param color a glm::vec3 that specified color value for the tile.
		\param tag an int that contains the target tag of the tile.
	*/
	Tile(float size, glm::vec3 color, int tag);

	//! A deconstructor
	~Tile();

	//! Sets the position of the tile.
	/*!
		\param position a glm::vec3 that contains the target position for the tile.
	*/
	void SetPosition(glm::vec3 position);

	//! Sets the owner of the tile. (Used for Tic-Tac-Toe)
	/*!
		\param o an int that contains the target owner.
	*/
	void SetOwner(int o)
	{
		owner = o;
	}

	//! Returns the owner of the tile. (Used for Tic-Tac-Toe)
	int GetOwner()
	{
		return owner;
	}

	//! Returns true if tile has drawn x or o. (Used for Tic-Tac-Toe)
	bool hasDrawn = false;
private:

	//! Contains the owner of the tile. Is initially -1.
	int owner = -1;

	//! Initializes tile
	/*!
		\param size a float that specifies the size of the tile.
		\param color a glm::vec3 that specified color value for the tile.
	*/
	void initializeTile(float size, glm::vec3 color);
};

#endif

