#include "CubeRenderer.h"
#include "GameObject.h"
#include <iostream>

CubeRenderer::CubeRenderer()
{
	m_ComponentName = "CubeRenderer Component";
	m_ComponentID = CubeRendererComponent;
}
CubeRenderer::CubeRenderer(glm::vec3 color, float size)
{
	m_ComponentName = "CubeRenderer Component";
	m_ComponentID = CubeRendererComponent;
	initializeMesh(color, size);
}


CubeRenderer::~CubeRenderer()
{
}

void CubeRenderer::updateComponent()
{
	if (m_GameObject->getParent()->hasChild("CameraObject"))
	{
		if (m_GameObject->getParent()->getChild("CameraObject")->hasComponent(CameraComponent))
		{
			m_Camera = m_GameObject->getParent()->getChild("CameraObject")->getComponent<Camera>(CameraComponent);
			renderCube(*m_Camera);
		}
	}

	// Renders Box
	if (m_GameObject->getParent()->hasComponent(CubeRendererComponent))
	{
		if (m_GameObject->getParent()->getComponent<CubeRenderer>(CubeRendererComponent)->hasCamera())
			renderCube(*m_GameObject->getParent()->getComponent<CubeRenderer>(CubeRendererComponent)->getCamera());
	}

	// Update Position 
	m_transform.GetPos()->x = m_GameObject->getComponent<Transform>(TransformComponent)->position.x;
	m_transform.GetPos()->y = m_GameObject->getComponent<Transform>(TransformComponent)->position.y;
	m_transform.GetPos()->z = m_GameObject->getComponent<Transform>(TransformComponent)->position.z;

	// Update Rotation
	m_transform.GetRot()->x = m_GameObject->getComponent<Transform>(TransformComponent)->rotation.x;
	m_transform.GetRot()->y = m_GameObject->getComponent<Transform>(TransformComponent)->rotation.y;
	m_transform.GetRot()->z = m_GameObject->getComponent<Transform>(TransformComponent)->rotation.z;

	// Update Scale
	m_transform.GetScale()->x = m_GameObject->getComponent<Transform>(TransformComponent)->scale.x;
	m_transform.GetScale()->y = m_GameObject->getComponent<Transform>(TransformComponent)->scale.y;
	m_transform.GetScale()->z = m_GameObject->getComponent<Transform>(TransformComponent)->scale.z;

}

void CubeRenderer::initializeMesh(glm::vec3 color, float size)
{
	m_color = color;
	// Vertex Buffer
	Vertex vertices[] =
	{
		// Triangle 1
		Vertex(glm::vec3( size,  size, size)),
		Vertex(glm::vec3(-size,  size, size)),
		Vertex(glm::vec3(-size, -size, size)),

		// Triangle 2
		Vertex(glm::vec3(-size, -size, size)),
		Vertex(glm::vec3( size, -size, size)),
		Vertex(glm::vec3( size,  size, size)),

		// Trinagle 3
		Vertex(glm::vec3(size,  size,  size)),
		Vertex(glm::vec3(size, -size,  size)),
		Vertex(glm::vec3(size, -size, -size)),

		// Triangle 4
		Vertex(glm::vec3(size, -size, -size)),
		Vertex(glm::vec3(size,  size, -size)),
		Vertex(glm::vec3(size,  size,  size)),

		// Triangle 5
		Vertex(glm::vec3( size, size,  size)),
		Vertex(glm::vec3( size, size, -size)),
		Vertex(glm::vec3(-size, size, -size)),

		// Triangle 6
		Vertex(glm::vec3(-size, size, -size)),
		Vertex(glm::vec3(-size, size,  size)),
		Vertex(glm::vec3( size, size,  size)),

		// Trinagle 7
		Vertex(glm::vec3(-size,  size,  size)),
		Vertex(glm::vec3(-size, -size,  size)),
		Vertex(glm::vec3(-size, -size, -size)),

		// Triangle 8
		Vertex(glm::vec3(-size, -size, -size)),
		Vertex(glm::vec3(-size,  size, -size)),
		Vertex(glm::vec3(-size,  size,  size)),

		// Triangle 9
		Vertex(glm::vec3( size,  size, -size)),
		Vertex(glm::vec3(-size,  size, -size)),
		Vertex(glm::vec3(-size, -size, -size)),

		// Triangle 10
		Vertex(glm::vec3(-size, -size, -size)),
		Vertex(glm::vec3( size, -size, -size)),
		Vertex(glm::vec3( size,  size, -size)),

		// Triangle 11
		Vertex(glm::vec3( size, -size,  size)),
		Vertex(glm::vec3( size, -size, -size)),
		Vertex(glm::vec3(-size, -size, -size)),

		// Triangle 12
		Vertex(glm::vec3(-size, -size, -size)),
		Vertex(glm::vec3(-size, -size,  size)),
		Vertex(glm::vec3( size, -size,  size)),
	};

	// Initialize
	m_mesh.init(vertices, sizeof(vertices) / sizeof(vertices[0]));
	m_shader.init((char*)"res/basicShader2");
}

void CubeRenderer::renderCube(const Camera& camera)
{
	m_shader.bind();
	m_shader.update(m_transform, camera, m_color);
	m_mesh.draw();
}

void CubeRenderer::onDestroy()
{

}

