#pragma once
#ifndef _PREFAB_H_
#define _PREFAB_H_

#include "GameObject.h"

//! A GameObject that allows all instances to have the same properties.
class Prefab : public GameObject
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Creates a new prefab.
		\param prefabName a const string& that contains the name of the prefab.
	*/
	Prefab(const string& prefabName);

	//! A deconstructor
	/*!
		Deallocates memory of the prefab during its lifetime.
	*/
	~Prefab();

#pragma endregion

#pragma region Public functions for Prefabs

	//! Saves a game object as a prefab.
	/*!
		\param gameObject a GameObject that contains the target object.
	*/
	void SaveToPrefab(GameObject gameObject);

	//! Instantiate a prefab.
	void InstantiatePrefab();

	//! Instantiate a prefab on a specified position.
	/*!
		\param position a glm::vec3 that contains the target x, y, and z position of the prefab.
	*/
	void InstantiatePrefab(glm::vec3 position); // overloaded

#pragma endregion

};

#endif

