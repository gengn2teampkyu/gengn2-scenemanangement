#include "NetworkManager.h"


NetworkManager::NetworkManager(unsigned short port)
{
	if (m_socket.bind(port) == sf::Socket::Done)
	{
		cout << "Network Manager has successfully reserved a port of address: " << port << endl;
		isAlive = true;
		m_systemPort = port;
		m_message[0] = '.'; // ASCI for Blank
	}
}


NetworkManager::~NetworkManager()
{
}

void NetworkManager::sendMessage(sf::IpAddress recipient, char message[1000])
{
	if (m_socket.send(message, 100, recipient, m_systemPort) == sf::Socket::Done)
		cout << " network is sending message to: " << recipient << endl;
}

void NetworkManager::threadRun()
{
	while (isAlive)
		receiveMessage();
}

void NetworkManager::receiveMessage()
{
	cout << "Network is receiving message" << endl;
	char dataReceived[1000];
	size_t received;
	sf::IpAddress sender;
	unsigned short port;

	if (m_socket.receive(dataReceived, 100, received, sender, port) == sf::Socket::Done)
	{
		m_mutex.lock();
		for (int i = 0; i < 1000; i++)
			m_message[i] = dataReceived[i];

		if (m_message[0] != '.') hasMessageReceived = true; // if message is not blank then set hasMessageReceived to True

		m_mutex.unlock();
	}
}

char* NetworkManager::getMessage()
{
	m_mutex.lock();
	for (int i = 0; i < 1000; i++)
		m_tempMessage[i] = m_message[i];
	m_mutex.unlock();
	return m_tempMessage;
}

void NetworkManager::closeNetwork()
{
	isAlive = false;
}

void NetworkManager::resetNetworkMessage()
{
	m_mutex.lock();
	m_message[0] = '.'; // reverts to ASCI blank
	hasMessageReceived = false;  // revert to false
	m_mutex.unlock();
}
