#pragma once
#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include "Component.h"
#include "Transform.h"
#include "Mesh.h"
#include "Camera.h"
#include "ModelTransform.h"
#include "Shader.h"
#include "CubeRenderer.h"
#include "Collider.h"

using namespace std;

//! Base class for all entities in the Engine.
class GameObject
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*! 
		Creates a new game object.
		A Transform component is always added to the game object.
		\sa Transform.h
	*/
	GameObject();

	//! A constructor
	/*! 
		Creates a new game object, named objectname
		A Transform component is always added to the game object.
		\param objectname a string that's used to set the name of the game object.
		\sa Transform.h
	*/
	GameObject(const string& objectname);

	//! A deconstructor
	/*!
		Deallocates memory of the game object during its lifetime.
	*/
	~GameObject();

	//! Initializes GameObject for classes who inherited from the GameObject.
	void initGameObject();

#pragma endregion

#pragma region Public functions for Component Based Design System

	//! Set the Tag of the game object.
	/*!
		\param gameObjectTag an int that contains the tag that will be set to the game object.
	*/
	void setTag(int gameObjectTag);

	//! Returns the tag of the game object.
	int getTag() const;

	//! Adds a component to the game object.
	/*!
		\param component a Component that will be added to the game object.
		\sa Component.h
	*/
	void addComponent(Component* component);

	//! Removes a component from the game object.
	/*!
		\param component a Component that will be removed from the game object.
		\sa Component.h
	*/
	void removeComponent(const int componentID);

	//! Returns the component with int componentID.
	/*!
		\param componentID an int that's used to specify which component is being called.
		\sa Component.h
	*/
	template<class  T>
	T* getComponent(int componentID)
	{
		Component* query = nullptr;
		for each (Component* component in m_components)
		{
			if (component->ComponentID() != componentID) continue;
			query = component; break;
		}
		//if (!query) throw invalid_argument("No Component Found!");
		if (!query) return nullptr;
		return (T*)query;
	}

	//! Returns if the game object contains the component with int ComponentID.
	/*!
		\param componentID an int that's used to specify which component is being called.
		\sa Component.h
	*/
	bool hasComponent(int ComponentID)
	{
		Component* comp = nullptr;
		for each (Component*  component in m_components)
		{
			if (component->ComponentID() != ComponentID) continue;
			comp = component; break;
		}
		if (!comp) return false;
		return true;
	}

	//! Returns the game object's list of components.
	vector<Component*> getComponentList(){ return m_components; }

#pragma endregion
	
#pragma region Public functions for Parenting System of GameObjects

	//! Adds a child game object to the game object.
	/*!
		\param child a GameObject that will be added to the game object.
	*/
	void addChild(GameObject* child);

	//! Removes a child game object from the game object.
	/*!
		\param child a GameObject that will be removed from the game object.
	*/
	void removeChild(const string& child);

	//! Returns the child game object of the game object.
	/*!
		\param childName a string that will be used to search for the child game object.
	*/
	GameObject* getChild(const string& childName)
	{
		GameObject* child = nullptr;
		for each (GameObject*  var in m_childObjects)
		{
			if (var->Name() != childName) continue;
			child = var; break;
		}
		if (!child) return nullptr;
		return child;
	}

	//! Returns the children of the game object.
	vector<GameObject*> getChildren()
	{
		return m_childObjects;
	}

	//! Returns if the game object contains the child with string childID.
	/*!
		\param childName an const string& that's used to specify which child is being called.
	*/
	bool hasChild(const string& childName)
	{
		GameObject* child = nullptr;
		for each (GameObject*  var in m_childObjects)
		{
			if (var->Name() != childName) continue;
			child = var; break;
		}
		if (!child) return false;
		return true;
	}

	//! Sets the game object's parent.
	/*
		\param parent a GameObject* that will be set as the parent of the game object.
	*/
	void setParent(GameObject* parent);

	//! Returns the game object's parent.
	GameObject* getParent() const
	{
		if (m_parent == nullptr) return nullptr;
		return m_parent;
	}

#pragma endregion

#pragma region Virtual functions for GameObjects

	//! Updates the game object and the component and children inside it.
	virtual void Update();
	//! Runs the content when enabled.
	virtual void OnEnable();
	//! Runs the content when disabled.
	virtual void OnDisable();

#pragma endregion

#pragma region Other Public functions and members for GameObjects

	//! Sets the name of the game object.
	/*!
		\param name a string that's used to set the name of the game object.
	*/
	void setName(const string& name);

	// Getters
	//! Returns the name of the game object.
	string Name() const;

	//! If the game object's rendering is allowed.
	bool EnableRender;

	//! Called when the game object is destroyed.
	void onGameObjectDestroy();
#pragma endregion

protected:

#pragma region Protected vectors for GameObjects

	//! Vector of Components
	vector<Component*>  m_components;
	//! Vector of Child GameObjects
	vector<GameObject*> m_childObjects; 

#pragma endregion

	//! Returns if update is enabled for the game object.
	bool m_EnableUpdate;

private:

#pragma region Private members for GameObjects

	//! The name of the object.
	string				m_gameOjectName;
	//! The tag of the object.
	int					m_gameObjectTag;
	//! The parent of the object.
	GameObject*			m_parent;
#pragma endregion

	//! Removes any kind of element in any collection object.
	template <typename C>
	inline void removeElement(vector<C>& vec, const C& element)
	{
		//vec.erase(remove(vec.begin(), vec.end(), element), vec.end());
		vector<C>::iterator position = find(vec.begin(), vec.end(), element);
		if (position != vec.end())
			vec.erase(position);
	}
};
#endif

