#ifndef _TAGS_DEFINITIONS_H_
#define _TAGS_DEFINITIONS_H_

// Sliding Game Tags
#define SLIDING_TAG 100
#define NORMAL_TAG 101
#define UNPASSABLE_TAG 102
#define FINALGOAL_TAG 103
#define PLAYERINITPOS_TAG 104

// Component Tags
#define TransformComponent 0
#define CameraComponent 1
#define CubeRendererComponent 2
#define SoundComponent 3
#define TextRendererComponent 4
#define ModelRendererComponent 5
#define ColliderComponent 6

#endif