#include "Tile.h"


Tile::Tile(float size, glm::vec3 color, int tag)
{
	setName("TileSample");
	initGameObject();
	Collider* colliderComponent = new Collider(size);
	CubeRenderer* cubeRendererComponent = new CubeRenderer(color, size);
	addComponent(cubeRendererComponent);
	addComponent(colliderComponent);
	setTag(tag);
	cout << getTag() << endl;
	initializeTile(size, color);
}


Tile::~Tile()
{
}

void Tile::SetPosition(glm::vec3 position)
{
	getComponent<Transform>(TransformComponent)->SetPosition(position);
}

void Tile::initializeTile(float size, glm::vec3 color)
{
	getComponent<CubeRenderer>(CubeRendererComponent)->initializeMesh(color, size);
}

