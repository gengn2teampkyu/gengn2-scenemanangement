#ifndef COLLIDER_H
#define COLLIDER_H


#include "Component.h"
#include <iostream>
#include <SFML\OpenGL.hpp>

using namespace std;

//! A component that creates a collider
class Collider: public Component
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Creates a new Collider Component
		\param size a float that contains the specified size of the Collider.
	*/
	Collider(float size);

	//! A deconstructor
	/*!
		Deallocates memory of the Collider during its lifetime.
	*/
	~Collider();

#pragma endregion

#pragma region Getters for Colliders

	//! Returns the x value of the collider
	float GetX();
	//! Returns the y value of the collider
	float GetY();
	//! Returns the z value of the collider
	float GetZ();

	//! Returns the side of the collider
	float GetSide();

#pragma endregion

#pragma region Public functions for Colliders

	//! Checks the collision of the collider with a GameObject that contains the specified tag
	/*!
		\param tag an int that contains the tag of the GameObject
	*/
	bool CheckCollision(int tag);
	//! Checks the collision of the collider
	bool CheckCollision();
	//! Adjusts the position of the collider
	void AdjustPosition(Vector3* adjustment);
	
	//! Updates the component
	void updateComponent() override;

	//! Returns the collider collided with
	/*!
		\param tag an int that contains the specified tag.
	*/
	Collider* OtherCollider(int tag);

#pragma endregion
	
private:
	
#pragma region Private members for Colliders

	//! Contains the value of the adjustment of the position of the Collider.
	Vector3 positionAdjust;

	//! Contains the size of the Collider.
	float size;

	//! Calls when destroyed
	void onDestroy() override;

#pragma endregion

};

#endif