#pragma once
#ifndef _NETWORK_MANAGER_H_
#define _NETWORK_MANAGER_H_

#include <SFML\System.hpp>
#include <SFML\Network.hpp>
#include <iostream>

using namespace std;
//! A class used to create a Network Manager.
class NetworkManager
{
public:

#pragma region Contstructor and Deconstructor

	//! A constructor
	/*!
		Creates a new Network Manager.
		\param port an unsigned short that contains the port to be reserved for an address.
	*/
	NetworkManager(unsigned short port);

	//! A deconstructor
	/*!
		Deallocates memory of the Network manager during its lifetime.
	*/
	~NetworkManager();

#pragma endregion

#pragma region Public functions 

	// Networking functions
	//! Sends message to a specific address
	/*!
		\param recipien an sf::IpAddress that contains the IP Address of the recipient
		\param message a char that contains the message to be sent.
	*/
	void sendMessage(sf::IpAddress recipient, char message[1000]);

	//! Runs the thread and which allows the Network Manager to receive a message while it's alive.
	void threadRun();

	//! Returns the message received by the Network Manager
	char* getMessage();

	//! Closes the Network Manager
	void closeNetwork();

	//! Resets the Network Manager to its initial state
	void resetNetworkMessage();
	
#pragma endregion

#pragma region Public members for NetworkManagers

	//! Is there a message?
	bool hasMessageReceived;

#pragma endregion

private:
	
#pragma region Private members for NetworkManagers

	//! Contains the Socket used by the NetworkManager
	sf::UdpSocket m_socket;

	//! Contains the Mutex used by the NetworkManager
	sf::Mutex	  m_mutex;

	//! Contains the System Port used by the NetworkManager
	unsigned short m_systemPort;

	//! Contains the message received
	char m_message[1000];

	//! Contains the temp message received
	char m_tempMessage[1000];

	//! Is the NetworkManager alive?
	bool isAlive;

#pragma endregion

#pragma region Private functions for NetworkManagers

	//! Receives message from another Network
	void receiveMessage();

#pragma endregion

};

#endif