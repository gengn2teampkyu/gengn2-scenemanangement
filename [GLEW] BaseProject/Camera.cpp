#include "Camera.h"
#include "GameObject.h"

Camera::Camera(const glm::vec3& pos,float fov, float aspect, float znear, float zfar)
{
	// Initialize Component Defaults
	m_ComponentName = "Camera Component";
	m_ComponentID = CameraComponent;

    m_prespective = glm::perspective(fov, aspect, znear, zfar);
    m_position = pos;
    m_forward = glm::vec3(0, 0, -1);
    m_up = glm::vec3(0, 1, 0);
}

glm::mat4 Camera::GetViewProjection() const
{
    return m_prespective * glm::lookAt(m_position,m_position+m_forward,m_up);
}

void Camera::updateComponent()
{
	m_position.x = m_GameObject->getComponent<Transform>(TransformComponent)->position.x;
	m_position.y = m_GameObject->getComponent<Transform>(TransformComponent)->position.y;
	m_position.z = m_GameObject->getComponent<Transform>(TransformComponent)->position.z;
}

void Camera::onDestroy()
{

}
