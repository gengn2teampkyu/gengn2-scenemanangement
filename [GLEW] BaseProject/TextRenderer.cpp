#include "TextRenderer.h"
#include <iostream>
#include "GameObject.h"

using namespace std;

TextRenderer::TextRenderer()
{
	m_ComponentName = "Text Renderer Component";
	m_ComponentID = TextRendererComponent;
}


TextRenderer::~TextRenderer()
{

}

void TextRenderer::updateComponent()
{
	//TODO: Draw the text using draw and get the Access Window 
	//renderText();
}

void TextRenderer::setFont(const string& fontDirectory)
{
	if (!m_font.loadFromFile(sf::String("Assets/Fonts/ubuntu.ttf"))) return;
	m_text.setFont(m_font);
}

void TextRenderer::setFontSize(float size)
{
	m_text.setCharacterSize(size);
}

void TextRenderer::setString(const string& textToDisplay)
{
	m_text.setString(textToDisplay);
}

void TextRenderer::setPosition(glm::vec2 pos)
{
	m_text.setPosition(pos.x, pos.y);
}

void TextRenderer::setColor(sf::Color color)
{
	m_text.setColor(color);
}

void TextRenderer::renderText(sf::RenderWindow& window)
{
	//window.draw(m_text);
}

void TextRenderer::initializeText(const string& fontDirectory, float size, glm::vec2 pos, sf::Color color, const string& textToDisplay)
{
	if (!m_font.loadFromFile(sf::String("Assets/Fonts/ubuntu.ttf"))) return;
	m_text.setFont(m_font);
	m_text.setCharacterSize(size);
	m_text.setPosition(pos.x, pos.y);
	m_text.setColor(color);
	m_text.setString(textToDisplay);
}
