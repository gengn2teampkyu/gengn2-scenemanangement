#include "PKYUEngine.h" // PKYU Engine Library

void main()
{
	SceneManager& sceneManager = SceneManager::getInstance();

	// Creation of Sliding Game Scene
	SlidingGame* slidingGame = new SlidingGame("PKYU Engine - Top Down View Sliding Game");
	sceneManager.AddToSceneList(slidingGame);

	// Creation of Tick Tack Toe Scene
	TickTackToe* ticktacktoeGame = new TickTackToe("PKYU Engine - TickTackToe Game");
	sceneManager.AddToSceneList(ticktacktoeGame);

	// Creation of PKYU Demo Scene
	DemoPKYU* demoPKYU = new DemoPKYU("Demo PKYU - Engine Features Demo");
	sceneManager.AddToSceneList(demoPKYU);

	// Load current scene
	sceneManager.LoadScene(2);
}