#include "Player.h"


enum Vertical
{
	UP,
	DOWN,
	ySTOP
};

enum Horizontal
{
	LEFT,
	RIGHT,
	xSTOP
};

Vertical yMovement;
Horizontal xMovement;

Player::Player()
{
	initGameObject();
}


Player::~Player()
{
}

void Player::MoveHorizontal(float dir)
{
	direction.x = dir;

	if (this->getComponent<Transform>(TransformComponent)->position.x < -0.1f)
	{
		if (direction.x < 0.0f) direction.x = 0.0f;
	}

	if (this->getComponent<Transform>(TransformComponent)->position.x > 4.0f)
	{
		if (direction.x > 0.0f) direction.x = 0.0f;
	}

	direction.y = 0;
	moving = true;
}

void Player::MoveVertical(float dir)
{
	direction.y = dir;

	if (this->getComponent<Transform>(TransformComponent)->position.y < -0.1f)
	{
		if (direction.y < 0.0f) direction.y = 0.0f;
	}

	if (this->getComponent<Transform>(TransformComponent)->position.y > 10.0f)
	{
		if (direction.y > 0.0f) direction.y = 0.0f;
	}

	direction.x = 0;
	moving = true;
}

void Player::Stop()
{
	if (moving)
	{
		direction.x = 0;
		direction.y = 0;
	}
}

void Player::Update()
{
	this->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(direction.x + acceleration.x, direction.y + acceleration.y, direction.z));

	if (this->getComponent<Transform>(TransformComponent)->position.x <= -0.1f)
	{
		if (direction.x < 0.0f) direction.x = 0.0f;
	}

	if (this->getComponent<Transform>(TransformComponent)->position.x >= 4.0f)
	{
		if (direction.x > 0.0f) direction.x = 0.0f;
	}

	if (this->getComponent<Transform>(TransformComponent)->position.y <= -0.1f)
	{
		if (direction.y < 0.0f) direction.y = 0.0f;
	}

	if (this->getComponent<Transform>(TransformComponent)->position.y >= 10.0f)
	{
		if (direction.y > 0.0f) direction.y = 0.0f;
	}

	if (this->getComponent<Collider>(ColliderComponent)->CheckCollision(NORMAL_TAG)|| this->getComponent<Collider>(ColliderComponent)->CheckCollision(PLAYERINITPOS_TAG))
	{
		if (moving == false && sliding == false)
		{
			acceleration.x = 0;
			acceleration.y = 0;
			direction.x = 0;
			direction.y = 0;
		}
	}

	if (this->getComponent<Collider>(ColliderComponent)->CheckCollision(SLIDING_TAG) &&
		(this->getComponent<Collider>(ColliderComponent)->CheckCollision(SLIDING_TAG) || this->getComponent<Collider>(ColliderComponent)->CheckCollision(UNPASSABLE_TAG)) &&
		(this->getComponent<Collider>(ColliderComponent)->CheckCollision(SLIDING_TAG) || this->getComponent<Collider>(ColliderComponent)->CheckCollision(NORMAL_TAG)))
	{
		moving = false;
		sliding = true;

		if (direction.x > 0.0f)
		{
			if (acceleration.x < 0.1f)	acceleration.x += 0.01f;
		}
		if (direction.x < 0.0f)
		{
			if (acceleration.x > -0.1f)	acceleration.x += -0.01f;
		}
		if (direction.y > 0.0f)
		{
			if (acceleration.y < 0.1f)	acceleration.y += 0.01f;
		}
		if (direction.y < 0.0f)
		{
			if (acceleration.y > -0.1f)	acceleration.y += -0.01f;
		}

		if (this->getComponent<Transform>(TransformComponent)->position.x < -0.1f || this->getComponent<Transform>(TransformComponent)->position.x >= 4.0f)
		{
			acceleration.x = 0.0f;
		}

		if (this->getComponent<Transform>(TransformComponent)->position.y < -0.1f || this->getComponent<Transform>(TransformComponent)->position.y > 10.0f)
		{
			acceleration.y = 0.0f;
		}
	}
	else
	{
		acceleration.x = 0;
		acceleration.y = 0;
		sliding = false;
	}

	if (this->getComponent<Collider>(ColliderComponent)->CheckCollision(UNPASSABLE_TAG))
	{
		float tileX = this->getComponent<Collider>(ColliderComponent)->OtherCollider(UNPASSABLE_TAG)->GetX();
		float tileY = this->getComponent<Collider>(ColliderComponent)->OtherCollider(UNPASSABLE_TAG)->GetY();

		acceleration.x = 0;
		acceleration.y = 0;

		if (direction.x > 0.0f && !yColliding)
		{
			if (!xMovement == Horizontal::RIGHT)
			{
				this->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(
					tileX - 0.75f,
					this->getComponent<Transform>(TransformComponent)->position.y,
					this->getComponent<Transform>(TransformComponent)->position.z));

				xColliding = true;
			}
			xMovement = Horizontal::LEFT;
		}

		else if (direction.x < 0.0f && !yColliding)
		{
			if (!xMovement == Horizontal::LEFT)
			{
				this->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(
					tileX + 0.75f,
					this->getComponent<Transform>(TransformComponent)->position.y,
					this->getComponent<Transform>(TransformComponent)->position.z));
				xColliding = true;
			}

			xMovement = Horizontal::RIGHT;
		}


		if (direction.y > 0 && !xColliding)
		{
			if (!yMovement == Vertical::UP)
			{
				this->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(
					this->getComponent<Transform>(TransformComponent)->position.x,
					tileY - 0.75f,
					this->getComponent<Transform>(TransformComponent)->position.z));
				yColliding = true;
			}
			yMovement = Vertical::DOWN;
		}

		if (direction.y < 0 && !xColliding)
		{
			if (!yMovement == Vertical::DOWN)
			{
				this->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(
					this->getComponent<Transform>(TransformComponent)->position.x,
					tileY + 0.75f,
					this->getComponent<Transform>(TransformComponent)->position.z));
				yColliding = true;
			}
			yMovement = Vertical::UP;
		}
	}
	else
	{
		xColliding = false;
		yColliding = false;
	}
	GameObject::Update();
}
