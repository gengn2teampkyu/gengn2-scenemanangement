#pragma once
#ifndef _MODEL_RENDERER_H_
#define _MODEL_RENDERER_H_

#include <glm\glm.hpp>
#include "ModelTransform.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"
#include "Camera.h"
#include "Vertex.h"
#include "Component.h"

//! A component that renders model files
class ModelRenderer : public Component
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Contains set default component name and ID.
	*/
	ModelRenderer();

	//! A deconstructor
	/*!
		Deallocates the memory of the component during its lifetime.
	*/
	~ModelRenderer();

#pragma endregion

#pragma region Public functions for ModelRenderers

	//! Loads model with texture.
	/*!
		\param modelFileName a const string& that contains the file name of the model.
		\param textureFileName a const string& that contains the file name of the texture.
	*/
	void loadModel(const string& modelFileName, const string& textureFileName); // Use for one line initialization of Model
	
	//! Loads model.
	/*!
		\param modelFileName a const string& that contains the file name of the model.
	*/
	void loadTextureFile(const string& fileName); // Use for loading the Texture file
	
	//! Loads texture.
	/*!
		\param textureFileName a const string& that contains the file name of the texture.
	*/
	void loadModel(const string& fileName); // Use for loading the obj / mesh file
	
	//! Updates the component
	void updateComponent() override;

	//! Returns if the component contains a camera.
	bool hasCamera(){return (m_Camera == NULL);}

	//! Returns the camera used.
	Camera* getCamera(){ return m_Camera; }

#pragma endregion

private:

#pragma region Private members for ModelRenderers

	//! Contains the ModelTransform of the ModelRenderer.
	ModelTransform m_transform;

	//! Contains the Shader of the ModelRenderer.
	Shader m_shader;

	//! Contains the Texture of the ModelRenderer
	Texture m_texture;

	//! Contains the Mesh of the ModelRenderer.
	Mesh m_mesh;

	//! Contains the Camera used by the ModelRenderer.
	Camera* m_Camera;

#pragma endregion

#pragma region Private functions for ModelRenderers

	//! Renders a mesh based on the Camera.
	/*!
		\param camera a const Camera& that contains the camera used for rendering a mesh.
	*/
	void renderModel(const Camera& camera); // Renders model;

	//! Calls when destroyed.
	void onDestroy() override;

#pragma endregion

};
#endif

