#pragma once
#ifndef _BACKGROUNDLAYER_H_
#define _BACKGROUNDLAYER_H_

#include "Scene.h"
#include <iostream>
#include <vector>

using namespace std;

class Layer : public GameObject
{
private:
	vector <GameObject*> sceneObjects;
	
public:
	void AddObject(GameObject* go);
	bool Enable();
	Layer();
	Layer(string name);
	~Layer();
};

#endif