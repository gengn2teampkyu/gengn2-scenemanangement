#pragma once
#ifndef _COMPONENT_H_
#define _COMPONENT_H_

#include <string>
#include <iostream>

using namespace std;

class GameObject;

//! Base class for everything attached to GameObjects
class Component
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Contains set default component name and ID.
	*/
	Component();

	//! A deconstructor
	/*!
		Deallocates the memory of the component during its lifetime.
	*/
	~Component();

#pragma endregion

#pragma region Getters for Components

	//! Returns the name of the component.
	string ComponentName() const;

	//! Returns the ID of the component.
	int ComponentID() const;

#pragma endregion

#pragma region Public functions for Components

	//! Sets the game object to which the component was attached to.
	/*!
		\param object a GameObject* that used to specify which game object was the component attached to.
	*/
	void SetGameObject(GameObject* object);

#pragma endregion

#pragma region Virtual functions for Components

	//! Updates the component.
	virtual void updateComponent() = 0;

	//! Called when an component is destroyed.
	virtual void onDestroy() = 0;
	
#pragma endregion

protected:

#pragma region Protected members for Components

	//! The name of the component.
	string m_ComponentName;
	//! The ID of the component.
	int m_ComponentID;
	//! The game object to which the component is attached to.
	GameObject* m_GameObject;

#pragma endregion
	
};
#endif

