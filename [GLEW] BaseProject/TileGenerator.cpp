#include "TileGenerator.h"


TileGenerator::TileGenerator()
{
	
}

TileGenerator::TileGenerator(char fileName[])
{
	initializeTiles(fileName);
	m_Position.x = 0;
	m_Position.y = 0;
	m_Position.z = 0;
}


TileGenerator::~TileGenerator()
{
}

void TileGenerator::initializeTiles(char fileName[])
{
	ifstream textFile;
	textFile.open(fileName);

	vector<char> line;
	if (!textFile.fail())
	{
		char type;
		do
		{
			type = textFile.get();
			if (type == '\n' || type == -1)
			{
				if (line.size() > 0)
				{
					tileContent.push_back(line);
					line.erase(line.begin(), line.end());
				}
			}
			else line.push_back(type);
		} while (textFile.good());
	}

	// Tile Generation
	for (int i = 0; i < tileContent.size(); i++)
	{
		for (int j = 0; j < tileContent[i].size(); j++)
		{
			switch (tileContent[i][j])
			{
				case 's':
				{
					cout << "Generating Sliding Tile" << endl;
					m_Tiles.push_back(new Tile(0.5f,glm::vec3(0.0f, 0.0f, 1.0f), SLIDING_TAG)); // Blue
				} break;
				case 'n':
				{
					cout << "Generating Normal Tile" << endl;
					m_Tiles.push_back(new Tile(0.5f, glm::vec3(1.0f, 1.0f, 0.0f), NORMAL_TAG)); // Yellow
				} break;
				case 'x':
				{
					cout << "Generating Non-Passable Tile" << endl;
					m_Tiles.push_back(new Tile(0.5f, glm::vec3(0.0f, 1.0f, 0.0f), UNPASSABLE_TAG)); // Green
				} break;
				case 'f':
				{
					cout << "Generating Final Goal Tile" << endl;
					m_Tiles.push_back(new Tile(0.5f, glm::vec3(0.25f, 0.0f, 0.0f), FINALGOAL_TAG)); // Red
				} break;
				
				case 'p':
				{
					cout << "Generating Player Initial Position Tile" << endl;
					m_Tiles.push_back(new Tile(0.5f, glm::vec3(0.5f, 0.0f, 0.0f), PLAYERINITPOS_TAG)); // Red
				} break;
			}
			
			m_Tiles.back()->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(m_Position.x + j * 1.0f, m_Position.y + (tileContent.size() - i - 1.0f), 0.0f));

			if (tileContent[i][j] == 'p')
			{
				playerPosition.x = m_Tiles.back()->getComponent<Transform>(TransformComponent)->position.x;
				playerPosition.y = m_Tiles.back()->getComponent<Transform>(TransformComponent)->position.y;
				//playerPosition.z = m_Tiles.back()->getComponent<Transform>(TransformComponent)->position.z;
			}
		}
	}
}
