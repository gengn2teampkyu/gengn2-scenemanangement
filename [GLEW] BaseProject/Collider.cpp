
#include <iostream>
#include <vector>
#include "GameObject.h"
#include "Scene.h"
#include "Collider.h"

using namespace std;

Collider::Collider(float s)
{
	size = s;
	m_ComponentName = "Collider Component";
	m_ComponentID = ColliderComponent;
}

float Collider::GetX()
{
	return m_GameObject->getComponent<Transform>(TransformComponent)->position.x + positionAdjust.x;
}

float Collider::GetY()
{
	return m_GameObject->getComponent<Transform>(TransformComponent)->position.y + positionAdjust.y;
}

float Collider::GetZ()
{
	return m_GameObject->getComponent<Transform>(TransformComponent)->position.z + positionAdjust.z;
}

float Collider::GetSide()
{
	return size;
}

bool Collider::CheckCollision()
{
	vector<GameObject*> collidables = m_GameObject->getParent()->getChildren();

	for each(GameObject* go in collidables)
	{
		Collider* other = NULL;

		if (go->hasComponent(6) && go != m_GameObject)
		{
			other = go->getComponent<Collider>(ColliderComponent);

			if ((GetX() - GetSide()) <= (other->GetX() + other->GetSide()) &&
				(other->GetX() - other->GetSide()) <= (GetX() + GetSide()) &&
				(GetY() - GetSide()) <= (other->GetY() + other->GetSide()) &&
				(other->GetY() - other->GetSide()) <= (GetY() + GetSide()))
			{
				return true;
			}
		}
	}

	return false;
}

bool Collider::CheckCollision(int tag)
{
	vector<GameObject*> collidables = m_GameObject->getParent()->getChildren();

	for each(GameObject* go in collidables)
	{
		Collider* other = NULL;
		if (go->hasComponent(6) && go != m_GameObject && go->getTag() == tag)
		{
			other = go->getComponent<Collider>(ColliderComponent);

			if ((GetX() - GetSide()) <= (other->GetX() + other->GetSide()) &&
				(other->GetX() - other->GetSide()) <= (GetX() + GetSide()) &&
				(GetY() - GetSide()) <= (other->GetY() + other->GetSide()) &&
				(other->GetY() - other->GetSide()) <= (GetY() + GetSide()))
			{
				return true;
			}
		}
	}

	return false;
}

Collider* Collider::OtherCollider(int tag)
{
	vector<GameObject*> collidables = m_GameObject->getParent()->getChildren();

	for each(GameObject* go in collidables)
	{
		Collider* other = NULL;
		if (go->hasComponent(6) && go != m_GameObject && go->getTag() == tag)
		{
			other = go->getComponent<Collider>(ColliderComponent);

			if ((GetX() - GetSide()) <= (other->GetX() + other->GetSide()) &&
				(other->GetX() - other->GetSide()) <= (GetX() + GetSide()) &&
				(GetY() - GetSide()) <= (other->GetY() + other->GetSide()) &&
				(other->GetY() - other->GetSide()) <= (GetY() + GetSide()))
			{
				return other;
			}
		}
	}
}

void Collider::updateComponent()
{
	glPushMatrix();
	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(size + positionAdjust.x, -size + positionAdjust.y, GetZ());
	glVertex3f(size + positionAdjust.x, size + positionAdjust.y, GetZ());
	glVertex3f(-size + positionAdjust.x, size + positionAdjust.y, GetZ());
	glVertex3f(-size + positionAdjust.x, -size + positionAdjust.y, GetZ());
	glEnd();
	glPopMatrix();
}

void Collider::AdjustPosition(Vector3* adjustment)
{
	positionAdjust = *adjustment;
}

void Collider::onDestroy()
{

}