#ifndef MODEL_TRANSFORM_H
#define MODEL_TRANSFORM_H

#include <glm/glm.hpp>
#define GLM_FORCE_RADIANS
#include <glm/gtx/transform.hpp>

//! A class that handles the transform of meshes and models
class ModelTransform
{
public:
	//! A constructor
	/*!
		Creates a model transform with initial values.
		\param pos a glm::vec3& that contains the position of the model.
		\param rot a glm::vec3& that contains the rotation of the model.
		\param scale a glm::vec3& that contains the scale of the model.
	*/
	ModelTransform(const glm::vec3& pos = glm::vec3(),
        const glm::vec3& rot = glm::vec3(),
        const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f));
	//! A deconstructor
	/*!
		Deallocates memory of the model transform during its lifetime.
	*/
	virtual ~ModelTransform();

	//! Returns position
    inline glm::vec3* GetPos(){ return &pos; }
	//! Returns rotation
	inline glm::vec3* GetRot() { return &rot; }
	//! Returns scale
	inline glm::vec3* GetScale() { return &scale; }

	//! Sets position
	/*!
		\param pos a glm::vec3& that contains the desired position.	
	*/
	inline void SetPos(glm::vec3& pos) { this->pos = pos; }

	//! Sets rotation
	/*!
		\param rot a glm::vec3& that contains the desired rotation.
	*/
	inline void SetRot(glm::vec3& rot) { this->rot = rot; }

	//! Sets scale
	/*!
		\param scale a glm::vec3& that contains the desired scale.
	*/
	inline void SetScale(glm::vec3& scale) { this->scale = scale; }

	//! Returns the assigned model.
    glm::mat4 getModel() const;
protected:
private:
    glm::vec3 pos;
    glm::vec3 rot;
    glm::vec3 scale;
};

#endif
