#ifndef _SCENE_1_H_
#define _SCENE_1_H_

#include "PKYUEngine.h"

class GameScene : public Scene
{
public:
	//CLONE FUNCTION FOR SCENE MANAGEMENT PURPOSES
	virtual GameScene* clone()
	{
		return(new GameScene(*this));
	}

	GameScene(string n) : Scene(n){} //CONSTRUCTOR INHERITANCE
	~GameScene(); //DESTRUCTOR

	//DECLARATIONS
	//----------------------------------------------------------------------------------------
	//GAMEOBJECT DECLARATION
	GameObject* sampleGameObject;
	GameObject* sampleGameObject2;
	GameObject* cameraGameObject;
	GameObject* sampleChildGameObject;

	//COMPONENT DECLARATION
	CubeRenderer* newmesh; //Mesh for sampleGameObject
	CubeRenderer* newmesh2; //Mesh for sampleGameObject2
	CubeRenderer* childmesh; //Mesh for sampleChildGameObject
	Collider* collider; //Collider for sampleGameObject
	Collider* collider2; //Collider for sampleGameObject2
	Camera* camera = new Camera(glm::vec3(0, 0, 0), 70.0f, 400.0f / 300.0f, 0.01f, 1000.0f); //Camera component for cameraGameObject
	//----------------------------------------------------------------------------------------

	bool add = false;

	void OnEnable() override
	{
		//if (sampleGameObject == NULL) throw invalid_argument("No Such Variable");
		//SET UP SCENE HERE
		//ASSIGN NEW INSTANCES TO GAMEOBJECTS AND COMPONENTS
		//------------------------------------------------------------------------------------
		//GAMEOBJECTS
		sampleGameObject = new GameObject("SampleObject");
		sampleGameObject2 = new GameObject("SampleObject2");
		cameraGameObject = new GameObject("CameraObject");
		sampleChildGameObject = new GameObject("ChildObject");
		//COMPONENTS
		newmesh = new CubeRenderer();
		newmesh2 = new CubeRenderer();
		collider = new Collider(1);
		collider2 = new Collider(1);
		childmesh = new CubeRenderer();
		camera = new Camera(glm::vec3(0, 0, 0), 70.0f, 400.0f / 300.0f, 0.01f, 1000.0f);
		//-------------------------------------------------------------------------------------

		sampleGameObject->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f)); //Sets sampleGameObject transform position
		sampleGameObject->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(1.0f, 1.0f, 1.0f)); //Sets sameGameObject transform scale
		sampleGameObject->setTag(SLIDING_TAG); //Sets samepleGameObject tag

		sampleGameObject2->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f)); //Sets sampleGameObject2 transform position
		sampleGameObject2->getComponent<Transform>(TransformComponent)->SetScale(glm::vec3(1.0f, 1.0f, 1.0f)); //Sets sameGameObject2 transform scale
		sampleGameObject2->setTag(SLIDING_TAG); //Sets samepleGameObject2 tag

		cameraGameObject->getComponent<Transform>(TransformComponent)->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f)); //Sets cameraGameObject transform position

		//ATTACH COMPONENTS TO GAMEOBJECTS
		//-------------------------------------------------------------------------------------
		sampleGameObject->addComponent(newmesh); //Attach newMesh to sampleGameObject
		sampleGameObject->addComponent(collider); //Attach collider to sampleGameObject

		sampleGameObject2->addComponent(newmesh2); //Attach newMesh to sampleGameObject2
		sampleGameObject2->addComponent(collider2); //Attach collider to sampleGameObject2

		cameraGameObject->addComponent(camera); //Attach camera to cameraGameObject

		sampleChildGameObject->addComponent(childmesh); //Attach mesh to sampleChildGameObject
		//--------------------------------------------------------------------------------------

		//ATTACH GAMEOBJECTS TO SCENE
		//--------------------------------------------------------------------------------------
		Scene::addChild(sampleGameObject); //Add sampleGameObject to GameScene as child
		Scene::addChild(sampleGameObject2); //Add sampleGameObject2 to GameScene as child
		Scene::addChild(cameraGameObject); //Add cameraGameObject to GameScene as child
		//--------------------------------------------------------------------------------------
	}

	void Start() override
	{
		cameraGameObject->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(0.0f, 0.0f, 2.5f));
		sampleGameObject->getComponent<CubeRenderer>(CubeRendererComponent)->initializeMesh(glm::vec3(0.5f, 0.5f, 0.0f), 0.090f);
		sampleGameObject2->getComponent<CubeRenderer>(CubeRendererComponent)->initializeMesh(glm::vec3(0.5, 0.5f, 0.0f), 0.090f);	
	}

	void Update() override
	{
		Scene::Update();

		sampleGameObject->getComponent<Collider>(ColliderComponent)->CheckCollision(1);
		sampleGameObject2->getComponent<Collider>(ColliderComponent)->CheckCollision();

		// Sample InputManager
		InputManager& inputManager = InputManager::getInstance();
		if (inputManager.num1KeyPressed())
			GetManager().LoadScene(0);
		if (inputManager.num2KeyPressed())
			GetManager().LoadScene(1);

		// TEST INPUT
		if (inputManager.escapeKeyPressed())
			sampleGameObject->removeChild("ChildObject"); // FIX
		if (inputManager.sKeyPressed())
			sampleGameObject->getComponent<Transform>(TransformComponent)->Scale(glm::vec3(1.0f, 1.0f, 1.0f));
		if (inputManager.wKeyPressed())
			sampleGameObject->getComponent<Transform>(TransformComponent)->Rotate(glm::vec3(100.0f, 0.0f, 0.0f));
		if (inputManager.dKeyPressed())
			sampleGameObject->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(0.1f, 0.0f, 0.0f));
		if (inputManager.aKeyPressed())
			sampleGameObject->getComponent<Transform>(TransformComponent)->Translate(glm::vec3(-0.1f, 0.0f, 0.0f));
	}
};
#endif