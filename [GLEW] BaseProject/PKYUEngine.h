#ifndef _PKYU_ENGINE_H_
#define _PKYU_ENGINE_H_

// Compile Code Windows

//g++ -DSFML_STATIC CubeRenderer.cpp ModelRenderer.cpp objLoader.cpp Texture.cpp Vertex.cpp AudioComponent.cpp Camera.cpp Collider.cpp Component.cpp Layer.cpp Mesh.cpp ModelTransform.cpp NetworkManager.cpp Shader.cpp TextRenderer.cpp Transform.cpp Tile.cpp TileGenerator.cpp Scene.cpp SceneManager.cpp GameObject.cpp InputManager.cpp main.cpp stb_image.c -o run.exe -I"C:/sfml/include" -L"C:/sfml/lib" -lopengl32 -lglu32 -lsfml-graphics -lsfml-window -lsfml-system
//g++ CubeRenderer.cpp ModelRenderer.cpp objLoader.cpp Texture.cpp Vertex.cpp AudioComponent.cpp Camera.cpp Collider.cpp Component.cpp Layer.cpp Mesh.cpp ModelTransform.cpp NetworkManager.cpp Shader.cpp TextRenderer.cpp Transform.cpp Tile.cpp TileGenerator.cpp Scene.cpp SceneManager.cpp GameObject.cpp InputManager.cpp main.cpp stb_image.c -o PKYUEngine.exe -IC:/glew/include -LC:/glew/lib -IC:/sfml/include -LC:/sfml/lib -IC:/glm -lsfml-window -lsfml-graphics -lsfml-system -lglew32  -lopengl32 -lglu32

// Header files
#include "GameObject.h"
#include "Component.h"
#include "SceneManager.h"
#include "Scene.h"
#include "Transform.h"
#include "CubeRenderer.h"
#include "InputManager.h"
#include "AudioComponent.h"
#include "TextRenderer.h"
#include "objLoader.h"
#include "Vertex.h"
#include "Texture.h"
#include "Shader.h"
#include "ModelRenderer.h"
#include "Collider.h"
#include "NetworkManager.h"
#include "Prefab.h"

// Tags and Definitions
#include "TagsDefinitions.h"

// Scenes
#include "GameScene.h"
#include "ExplorationScene.h"
#include "SlidingGame.h"
#include "TickTackToe.h"
#include "DemoPKYU.h"

// Libraries
#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\OpenGL.hpp>
#include <SFML\Window.hpp>

// Namespaces
using namespace std;

#endif