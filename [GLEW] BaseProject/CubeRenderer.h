#pragma once
#ifndef _MESH_RENDERER_H_
#define _MESH_RENDERER_H_

#include "Component.h"
#include "ModelTransform.h"
#include "Shader.h"
#include "Mesh.h"
#include "Camera.h"
#include <glm\glm.hpp>
#include "Vertex.h"
#include "TagsDefinitions.h" // Tags Library

//! A component that renders a GLEW Box
class CubeRenderer : public Component
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*!
		Contains set default component name and ID.
	*/
	CubeRenderer();

	//! A constructor
	/*!
		Contains set default component name and ID.
		\param color a glm::vec3 that is used to set the color of the mesh.
		\param size a float that indicates the size of the mesh.
	*/
	CubeRenderer(glm::vec3 color, float size);

	//! A deconstructor
	/*!
		Deallocates the memory of the component during its lifetime.
	*/
	~CubeRenderer();

#pragma endregion

#pragma region  Public functions of CubeRenderers

	//! Initializes Mesh
	/*!
		\param color a glm::vec3 that is used to set the color of the mesh.
		\param size a float that indicates the size of the mesh.
	*/
	void initializeMesh(glm::vec3 color, float size);

	//! Updates the component
	void updateComponent() override;

	//! Returns if the component contains a camera.
	bool hasCamera(){return (m_Camera == NULL);}

#pragma endregion

#pragma region Public members of CubeRenderers

	//! Contains the Camera used by the CubeRenderer.
	Camera* getCamera(){return m_Camera;}

#pragma endregion
	
private:

#pragma region Private members of CubeRenderers

	//! Contains the ModelTransform of the CubeRenderer.
	ModelTransform m_transform;

	//! Contains the Shader of the CubeRenderer.
	Shader m_shader;
	
	//! Contains the Mesh of the CubeRenderer.
	Mesh m_mesh;

	//! Contains the color value of the CubeRenderer.
	glm::vec3 m_color;

	//! Camera reference for the Mesh.
	Camera* m_Camera;

#pragma endregion

#pragma region Private functions of CubeRenderers

	//! Renders a cube based on the Camera.
	/*!
		\param camera a const Camera& that contains the camera used for rendering a cube.
	*/
	void renderCube(const Camera& camera);
	
	//! Calls when destroyed.
	void onDestroy() override;
	
#pragma endregion

};
#endif

