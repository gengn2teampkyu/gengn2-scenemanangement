#include "GameObject.h"

using namespace std;

GameObject::GameObject()
{
	m_EnableUpdate = true;
}
GameObject::GameObject(const string& objectname)
{
	m_gameOjectName = objectname;
	m_EnableUpdate = true;
	Transform* transformComponent = new Transform();
	addComponent(transformComponent);
}

void GameObject::initGameObject()
{
	Transform* transformComponent = new Transform();
	addComponent(transformComponent);
}

GameObject::~GameObject()
{
}

void GameObject::setTag(int gameObjectTag)
{
	m_gameObjectTag = gameObjectTag;
}

int GameObject::getTag() const
{
	return m_gameObjectTag;
}
void GameObject::setName(const string& name)
{
	m_gameOjectName = name;
}

string GameObject::Name() const
{
	return m_gameOjectName;
}

void GameObject::addComponent(Component* component)
{
	m_components.push_back(component);
	component->SetGameObject(this);
}

void GameObject::removeComponent(const int componentID)
{
	Component* compToRemove = nullptr;
	for each (Component* c in m_components)
	{
		if (c->ComponentID() != componentID) continue;
		compToRemove = c; break;
	}
	if (compToRemove == nullptr) throw invalid_argument("No Component In List!");

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 6);
	cout << "Removed: " << compToRemove->ComponentName() << " from components!" << endl;
	removeElement(m_components, compToRemove);
}

void GameObject::setParent(GameObject* parent)
{
	m_parent = parent;
	cout << m_gameOjectName << " has added " << parent->Name() << " as Parent" << endl;
}

void GameObject::addChild(GameObject* child)
{
	m_childObjects.push_back(child);
	child->setParent(this);
}

void GameObject::removeChild(const string& child)
{
	GameObject* childComp = nullptr;
	for each (GameObject*  var in m_childObjects)
	{
		if (var->Name() != child) continue;
		childComp = var; break;
	}
	if (!childComp) throw invalid_argument("No Child Object Found!");
	childComp->setParent(nullptr);
	removeElement(m_childObjects, childComp);
}

void GameObject::Update()
{
	if (!m_EnableUpdate) return;
	if (m_components.size() > 0)
		for (unsigned int i = 0; i < m_components.size(); i++)
			m_components[i]->updateComponent();

	if (m_childObjects.size() > 0)
		for (unsigned int i = 0; i < m_childObjects.size(); i++)
			m_childObjects[i]->Update();
}

void GameObject::OnEnable()
{
	if (m_childObjects.size() == 0) return;
	for (unsigned int i = 0; i < m_childObjects.size(); i++)
		m_childObjects[i]->OnEnable();
}

void GameObject::OnDisable()
{
	if (m_childObjects.size() == 0) return;
	for (unsigned int i = 0; i < m_childObjects.size(); i++)
		m_childObjects[i]->OnDisable();
}

void GameObject::onGameObjectDestroy()
{
	// Call onDestroy for Components
	for each (Component* var in m_components)
		var->onDestroy();
	// Deallocate Memory
	for each (GameObject* var in m_childObjects)
		var = nullptr;
	for each (Component* var in m_components)
		var = nullptr;
}