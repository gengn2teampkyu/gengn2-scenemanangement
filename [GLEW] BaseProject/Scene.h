#pragma once
#ifndef _SCENE_H_
#define _SCENE_H_

#include "GameObject.h"
#include "Layer.h"
#include <vector>
#include <GL\glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>

using namespace std;

class SceneManager;

//! A class used for scene Creation
class Scene : public GameObject
{
private:
	//vector <Layer*> sceneLayers;
	
#pragma region Private members for Scenes

	//! Contains the SceneManager of the Scene.
	SceneManager* manager;

	//! Is the Scene running?
	bool running;

#pragma endregion

#pragma region Private functions for Scenes

	//! Is the Scene initialized?
	bool Init();

	//! Flow of the Scene.
	void MainLoop();

	//! Runs the event for Scenes.
	void Event();

	//! Resizes the window of the Scene.
	/*!
		\param width an unsigned int that contains the desired width of the window.
		\param height an unsigned int that contains the desired height of the window.
	*/
	void resizeWindow(unsigned int width, unsigned int height);

#pragma endregion

public:
	
#pragma region Constructor and Deconstructor

	//! A constructor
	Scene();

	//! A constructor
	/*!
		\param n a string used to set the name of the Scene.
	*/
	Scene(string n);

	//! A deconstructor
	/*!
		Deallocates the memory used by the scene during its lifetime.
	*/
	~Scene();

#pragma endregion

#pragma region Virtual functions for Scenes

	//! Runs on the Start of the Scene
	virtual void Start(){};
	//! Runs per frame
	virtual void Update();

	//! Creates a new instance of the Scene
	virtual Scene* clone()
	{
		return(new Scene(*this));
	}

#pragma endregion

#pragma region Public functions for Scenes

	//! Renders the Scene
	void Render();
	
	//! Starts the engine
	void StartEngine();

	//! Removes all the content of the Scene
	void EndScene();

	//! Sets to which SceneManager the Scene is attached to.
	/*!
		param sc is a SceneManager* to whom the Scene will be attached to.
	*/
	void SetManager(SceneManager* sc);
	

	//! Returns the SceneManager where the Scene was attached.
	SceneManager& GetManager()
	{
		return *manager;
	}

#pragma endregion

#pragma region Public members for Scenes

	//! Contains the number of time the scene was loaded.
	int timesLoaded;

	//! Contains the color value of the background
	Vector4 BackgroundColor = Vector4(0, 0, 0, 0);

#pragma endregion

};

#endif