#include "Prefab.h"


Prefab::Prefab(const string& prefabName)
{
	setName(prefabName);
	m_EnableUpdate = false;
}


Prefab::~Prefab()
{
	for each (Component* var in m_components) delete var;
	for each (GameObject* go in m_childObjects) delete go;
}

void Prefab::SaveToPrefab(GameObject gameObject)
{
	cout << "Adding: " << gameObject.Name() << " as a Prefab!" << endl;
	for each (Component* component in gameObject.getComponentList())
		m_components.push_back(component);
	for each (GameObject* childGameObjects in gameObject.getChildren())
		m_childObjects.push_back(childGameObjects);

	// Deletes Transform Component
	//removeComponent(TransformComponent);
	// Adds new Transform Component
	//Transform* newTransformComponent = new Transform();
	//addComponent(newTransformComponent);
}

void Prefab::InstantiatePrefab()
{
	m_EnableUpdate = true;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 6);
	cout << "INSTANTIATED PREFAB: " << Name() << endl;
}

void Prefab::InstantiatePrefab(glm::vec3 position)
{
	m_EnableUpdate = true;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 6);
	cout << "INSTANTIATED PREFAB: " << Name() << endl;

	// Set Transform
	getComponent<Transform>(TransformComponent)->Translate(position);
}


