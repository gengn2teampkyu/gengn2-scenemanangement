#pragma once
#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "GameObject.h"
#include <SFML\Graphics.hpp>
#include <SFML\OpenGL.hpp>
#include <SFML\Window.hpp>

//! Player class used for Sliding Game
class Player :	public GameObject
{
public:
	//! A constructor
	Player();
	//! A deconstructor
	~Player();

	//! Updates the player
	void Update();

	//! Moves the player along a horizontal direction
	/*!
		\param dir a float that contains the x direction of the player.
	*/
	void MoveHorizontal(float dir);
	//! Moves the player along a vertical direction
	/*!
		\param dir a float that contains the y direction of the player.
	*/
	void MoveVertical(float dir);
	//! Stops the player movement
	void Stop();

	//! Returns true if player is moving
	bool isMoving() { return moving; };

	//! Is the player sliding?
	bool sliding;

private:
	//! Is the player moving?
	bool moving = true;
	//! Is the player colliding horizontally?
	bool xColliding;
	//! Is the player colliding vertically?
	bool yColliding;
	//! Contains the direction of the player
	Vector3 direction;
	//! Contains the acceleration of the player
	Vector3 acceleration;
};

#endif

