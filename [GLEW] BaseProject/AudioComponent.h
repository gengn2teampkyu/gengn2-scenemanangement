#pragma once
#ifndef _AUDIO_COMPONENT_H_
#define _ADUIO_COMPONENT_H_

#include <string>
#include <SFML\Audio.hpp>
#include <memory>
#include <iostream>
#include "Component.h"
#include "TagsDefinitions.h"

using namespace std;
//! A component used to load and play audio files.
class AudioComponent : public Component
{
public:
	enum SoundType
	{
		Music,
		SFX
	};

#pragma region Constructor and Deconstructor

	//! A constructor
	/*
		Creates a new Audio Component.
	*/
	AudioComponent();

	//! A deconstructor
	/*
		Deallocates memory of the Audio Component during its lifetime.
	*/
	~AudioComponent();

#pragma endregion

#pragma region Public functions for AudioComponents

	//! Initializes Audio Component.
	/*!
		\param name a string that contains the name of the sound/audio.
		\param type a SoundType that contains the type of the sound/audio. (Music, SFX)
		\param fileName a const string& that contains the fileName used to open a file.
		\param volume a float that contains the volume of the sound/audio.
		\param autoPlay a bool that specifies whether the sound will be played automatically.
	*/
	void initializeSound(string name, SoundType type, const string& fileName, float volume, bool autoPlay);
	
	//! Plays the initialized BG Music.
	void playBGMusic();

	//! Sets the volume of the BG Music.
	/*!
		\param volume a float that contains the volume of the sound/audio.
	*/
	void setBGVolume(float volume);

	//! Stops the initialized BG Music.
	void stopBGMPlayback();

	// Pauses BG Music (For Extension)
	void pausePlayback();

	//! Plays the initialized sound effect.
	void playSFX();
	//! Sets the volume of the sound effect.
	/*!
		\param volume a float that contains the volume of the sound/audio.
	*/
	void setSFXVolume(float volume);

	//! Updates the component
	void updateComponent() override;

#pragma endregion

private:

#pragma region Private members for AudioComponents

	//! Name of the AudioComponent
	string			m_SoundName;

	//! SoundType of the AudioComponent (Music, SFX)
	SoundType		m_type;

	//! Contains the Music
	unique_ptr<sf::Music> m_Music;

	//! Contains the SoundBuffer
	unique_ptr<sf::SoundBuffer> m_SoundBuffer;
	
	//! Contains the Sound
	sf::Sound m_Sound;

	//! Is the AudioComponent played automatically?
	bool m_autoPlay;

	//! Calls when destroyed
	void onDestroy() override;

#pragma endregion

};
#endif

