#include "Scene.h"
#include "SceneManager.h"

Scene::Scene()
{
}
Scene::Scene(string n)
{
	setName(n);
}
Scene::~Scene()
{
}

void Scene::Render()
{
	GetManager().AccessWindow()->display();
}

void Scene::Update()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(BackgroundColor.x, BackgroundColor.y, BackgroundColor.z, BackgroundColor.a);
	glLoadIdentity();
	GameObject::Update();
}

void Scene::MainLoop()
{
	GameObject::OnEnable();
	Start();
	while (GetManager().AccessWindow()->isOpen())
	{
		Update();
		Render();
	}
}
void Scene::SetManager(SceneManager* sc)
{
	manager = sc;
}

void Scene::EndScene()
{
	if (m_childObjects.size() == 0) return;
	for each(GameObject* child in m_childObjects)
	{
		child->onGameObjectDestroy();
	}

	m_childObjects.clear();
}