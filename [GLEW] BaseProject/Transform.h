#pragma once
#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include "Component.h"
#include "glm\glm.hpp"
#include <iostream>

#include "TagsDefinitions.h"

using namespace std;

//! A struct that represents 3D vectors and points
struct Vector3
{

#pragma region Members for Vector3

	//! X coordinate of the vector.
	float x;
	//! Y coordinate of the vector.
	float y;
	//! Z coordinate of the vector.
	float z;

#pragma endregion

#pragma region Constructor

	//! A constructor
	/*
		Creates an empty Vector3.
	*/
	Vector3()
	{

	}	
	//! A constructor
	/*
		Creates Vector3 and sets it position.
		\param xpos a float the contains the x position of the vector.
		\param ypos a float the contains the y position of the vector.
		\param zpos a float the contains the z position of the vector.
	*/
	Vector3(float xpos, float ypos, float zpos)
	{
		this->x = xpos;
		this->y = ypos; 
		this->z = zpos;
	}

#pragma endregion

public: 

#pragma region Public functions for Vector3

	//Vector3* Subract(Vector3* a, Vector3* b)
	//{
	//	Vector3* c = new Vector3(a->x - b->x, a->y - b->y, a->z - b->z);
	//	return c;
	//}

	//! Adds the value of another vector
	/*!
		\param vec a Vector3 that contains the target vector.
	*/
	void AddVector(Vector3 vec)
	{
		x += vec.x;
		y += vec.y;
		z += vec.z;
	}

	//! Subtracts the value of another vector
	/*!
		\param vec a Vector3 that contains the target vector.
	*/
	void SubtractVector(Vector3 vec)
	{
		x -= vec.x;
		y -= vec.y;
		z -= vec.z;
	}

	//! Returns the Distance between two Vectors
	/*!
		\param a a Vector3* that contains the first vector.
		\param b a Vector3* that contains the second vector.
		
	*/
	static float Distance(Vector3* a, Vector3* b)
	{
		Vector3* c = new Vector3(a->x - b->x, a->y - b->y, a->z - b->z);

		return sqrt((c->x*c->x) / 2 + (c->y*c->y) / 2 + (c->z*c->z) / 2);
	}

#pragma endregion

};

//! A struct that represents 4D vectors and points
struct Vector4
{

#pragma region Members for Vector4

	//! X coordinate of the vector.
	float x; 
	//! Y coordinate of the vector.
	float y;
	//! Z coordinate of the vector.
	float z;
	//! A coordinate of the vector.
	float a;

#pragma endregion

#pragma region Constructor

	//! A constructor
	/*
		Creates an empty Vector4.
	*/
	Vector4()
	{

	}
	//! A constructor
	/*
		Creates Vector4 and sets it position.
		\param xpos a float the contains the x position of the vector.
		\param ypos a float the contains the y position of the vector.
		\param zpos a float the contains the z position of the vector.
		\param apos a float the contains the a position of the vector.
	*/
	Vector4(float xpos, float ypos, float zpos, float apos)
	{
		this->x = xpos;
		this->y = ypos;
		this->z = zpos;
		this->a = apos;
	}

#pragma endregion

};

//! A component that contains the Position, Rotation, and Scale of an object.
class Transform : public Component
{
public:

#pragma region Constructor and Deconstructor

	//! A constructor
	/*
	Contains set default component name and ID.
	*/
	Transform();

	//! A deconstructor
	/*
	Deallocates the memory of the component during its lifetime.
	*/
	~Transform();

#pragma endregion

#pragma region Public functions for Tranform Components

	//! Updates the component
	void updateComponent() override;

	//! Moves the transform towards the direction.
	/*!
		\param direction a glm::vec3 that specifies the direction of the transform.
	*/
	void Translate(glm::vec3 direction);

	//! Rotates the transform towards the angle.
	/*!
		\param rotation a glm::vec3 that specifies the angle of rotation of the transform.
	*/
	void Rotate(glm::vec3 rotation);

	//! Scales the transform.
	/*!
		\param scale a glm::vec3 that specifies the scale of the transform.
	*/
	void Scale(glm::vec3 scale);

#pragma endregion

#pragma region Setters for Transform Components

	// Setters
	//! Sets the position of the transform.
	/*!
		\param pos a glm::vec3 that specifies the position.
	*/
	void SetPosition(glm::vec3 pos);
	//! Sets the rotation of the transform.
	/*!
		\param rot a glm::vec3 that specifies the rotation.
	*/
	void SetRotation(glm::vec3 rot);
	//! Sets the scale of the transform.
	/*!
		\param scale a glm::vec3 that specifies the scale.
	*/
	void SetScale(glm::vec3 scale);

#pragma endregion

#pragma region Public members for Transform Components

	//! The position of the transform.
	Vector3 position;
	//! The rotation of the transform.
	Vector3 rotation;
	//! The scale of the transform.
	Vector3 scale;

#pragma endregion

private:

#pragma region Private members for Transform Components

	//! Contains the position of the parent of the GameObject with this component.
	Vector3 parentPos;
	//! Contains the rotation of the parent of the GameObject with this component.
	Vector3 parentRot;
	//! Contains the scale of the parent of the GameObject with this component.
	Vector3 parentScale;

	//! Contains the current position of this component.
	Vector3 curPos;
	//! Contains the rotation position of this component.
	Vector3 curRot;
	//! Contains the scale position of this component.
	Vector3 curScale;

	//! Contains the x position of the Transform.
	float x;
	//! Contains the y position of the Transform.
	float y;
	//! Contains the z position of the Transform.
	float z;

	//! Contains the x rotation of the Transform.
	float xRot;
	//! Contains the y rotation of the Transform.
	float yRot;
	//! Contains the z rotation of the Transform.
	float zRot;

	//! Contains the x scale of the Transform.
	float xScale; 
	//! Contains the y scale of the Transform.
	float yScale;
	//! Contains the z scale of the Transform.
	float zScale;

#pragma endregion

	//! Loads the initial position of the transform.
	void loadDefaultModules();

	//! Calls when destroyed.
	void onDestroy() override;
};

#endif

