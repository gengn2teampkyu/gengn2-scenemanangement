var searchData=
[
  ['scale',['Scale',['../class_transform.html#a2b0584819816bcc3a790c5a6d7d69fb2',1,'Transform']]],
  ['setgameobject',['SetGameObject',['../class_component.html#ae58454d1404e4144f2ab0cccd0ec7d57',1,'Component']]],
  ['setname',['setName',['../class_game_object.html#a07df7041806a6c409b30b7dc817deedd',1,'GameObject']]],
  ['setparent',['setParent',['../class_game_object.html#a81dad461c1d10c1679b4e1dd9e67c425',1,'GameObject']]],
  ['setposition',['SetPosition',['../class_transform.html#ad4101aa9ff0004360c9ffec202a1ae3e',1,'Transform']]],
  ['setrotation',['SetRotation',['../class_transform.html#a1e2da8aa8e1e71f1d3a7f66cd5fd661c',1,'Transform']]],
  ['setscale',['SetScale',['../class_transform.html#a17725a6655e8da89b513de5b00ba077b',1,'Transform']]]
];
