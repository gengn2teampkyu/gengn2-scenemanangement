var searchData=
[
  ['gameobject',['GameObject',['../class_game_object.html',1,'GameObject'],['../class_game_object.html#a0348e3ee2e83d56eafca7a3547f432c4',1,'GameObject::GameObject()'],['../class_game_object.html#aa9d3b782a34bbcf6001d21689cac8495',1,'GameObject::GameObject(const string &amp;objectname)']]],
  ['getchild',['getChild',['../class_game_object.html#a2581c7f33cf99af98e762fab39f37269',1,'GameObject']]],
  ['getcomponent',['getComponent',['../class_game_object.html#a4cbbc36da76b82dba5d7919137988584',1,'GameObject']]],
  ['getparent',['getParent',['../class_game_object.html#ab3dfda6322e5d4568f1d7e1b1c7b6d02',1,'GameObject']]]
];
