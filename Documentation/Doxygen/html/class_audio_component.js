var class_audio_component =
[
    [ "SoundType", "class_audio_component.html#a442028f680215f6c4afc1d942e60a6b9", [
      [ "Music", "class_audio_component.html#a442028f680215f6c4afc1d942e60a6b9a8f5b1054ac50e36f1335fcc2c9aec93b", null ],
      [ "SFX", "class_audio_component.html#a442028f680215f6c4afc1d942e60a6b9af3f04a31ef96aeeca365ee32c38f9b67", null ]
    ] ],
    [ "AudioComponent", "class_audio_component.html#aaea64af7cd81778f6737f0a4bee35394", null ],
    [ "~AudioComponent", "class_audio_component.html#a68bf2351458d61601aad7abeb3f58588", null ],
    [ "initializeSound", "class_audio_component.html#aa0a6fbf98a3d40dac7c92925f6f17c66", null ],
    [ "pausePlayback", "class_audio_component.html#af87aecf9f8c2cb0db0af3e66130f803b", null ],
    [ "playBGMusic", "class_audio_component.html#aa3353d2d677f3029646f857e86107c16", null ],
    [ "playSFX", "class_audio_component.html#a22c50b5477352b1b122d044e222c7981", null ],
    [ "setBGVolume", "class_audio_component.html#ac68001cb5228c9acf1da02fe13f0412a", null ],
    [ "setSFXVolume", "class_audio_component.html#a8cac6c3fdee92e374ea7c4c8b9deaeb3", null ],
    [ "stopBGMPlayback", "class_audio_component.html#ae7de28daeb99059d36dbc171d0c84f2e", null ],
    [ "updateComponent", "class_audio_component.html#a3670a24bf159c134197109181a3e866f", null ]
];