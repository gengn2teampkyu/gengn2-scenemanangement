var class_component =
[
    [ "Component", "class_component.html#a8775db6d1a2c1afc2e77cd3c8f39da6f", null ],
    [ "~Component", "class_component.html#ab8378fa275af98e568a7e91d33d867af", null ],
    [ "ComponentID", "class_component.html#a7fa049dcccef56cbe68d3c923fb30b25", null ],
    [ "ComponentName", "class_component.html#a133656eb95505d280c00f8ac58996d9b", null ],
    [ "onDestroy", "class_component.html#a2b198f27162a6caf63917e304295f892", null ],
    [ "SetGameObject", "class_component.html#ae58454d1404e4144f2ab0cccd0ec7d57", null ],
    [ "updateComponent", "class_component.html#a92f26c4ddae79db7fbbeac1fe04e7861", null ],
    [ "m_ComponentID", "class_component.html#ab3025d1858616b3a919de96090bccb6d", null ],
    [ "m_ComponentName", "class_component.html#a55efd23aa8a826bbce73edf69e715b40", null ],
    [ "m_GameObject", "class_component.html#a3c0bfc748ab627f0490dc2da23fceabb", null ]
];