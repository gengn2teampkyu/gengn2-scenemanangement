var hierarchy =
[
    [ "Component", "class_component.html", [
      [ "AudioComponent", "class_audio_component.html", null ],
      [ "Camera", "class_camera.html", null ],
      [ "Collider", "class_collider.html", null ],
      [ "CubeRenderer", "class_cube_renderer.html", null ],
      [ "ModelRenderer", "class_model_renderer.html", null ],
      [ "TextRenderer", "class_text_renderer.html", null ],
      [ "Transform", "class_transform.html", null ]
    ] ],
    [ "GameObject", "class_game_object.html", [
      [ "Player", "class_player.html", null ],
      [ "Prefab", "class_prefab.html", null ],
      [ "Scene", "class_scene.html", null ],
      [ "SceneManager", "class_scene_manager.html", null ],
      [ "Tile", "class_tile.html", null ]
    ] ],
    [ "InputManager", "class_input_manager.html", null ],
    [ "Mesh", "class_mesh.html", null ],
    [ "ModelTransform", "class_model_transform.html", null ],
    [ "NetworkManager", "class_network_manager.html", null ],
    [ "Shader", "class_shader.html", null ],
    [ "Texture", "class_texture.html", null ],
    [ "TileGenerator", "class_tile_generator.html", null ],
    [ "Vector3", "struct_vector3.html", null ],
    [ "Vector4", "struct_vector4.html", null ],
    [ "Vertex", "class_vertex.html", null ]
];