var class_o_b_j_model =
[
    [ "OBJModel", "class_o_b_j_model.html#ac33fd4d7e20cd7ca5a82962015e91686", null ],
    [ "ToIndexedModel", "class_o_b_j_model.html#ad1e4ca6919c26a164aca9fa83523f142", null ],
    [ "hasNormals", "class_o_b_j_model.html#a306f4792cc8b11ffdae23de05e299b07", null ],
    [ "hasUVs", "class_o_b_j_model.html#a68c309623f6223858524180eac4c8dff", null ],
    [ "normals", "class_o_b_j_model.html#af61e5eb97529d47fe8e050fcd0bc976a", null ],
    [ "OBJIndices", "class_o_b_j_model.html#a6522870686168e757385b62214abe37b", null ],
    [ "uvs", "class_o_b_j_model.html#a66d03d734db51477fce847066d472993", null ],
    [ "vertices", "class_o_b_j_model.html#aa1444e5c0ec8249988d5b7a1f54297c6", null ]
];