var class_player =
[
    [ "Player", "class_player.html#affe0cc3cb714f6deb4e62f0c0d3f1fd8", null ],
    [ "~Player", "class_player.html#a749d2c00e1fe0f5c2746f7505a58c062", null ],
    [ "isMoving", "class_player.html#a6a0b5131321ade6c0e9e30869507b587", null ],
    [ "MoveHorizontal", "class_player.html#a389942fb923516ecce681df021843e34", null ],
    [ "MoveVertical", "class_player.html#a0a7874f6e58d6cb83e467fe40d4bb327", null ],
    [ "Stop", "class_player.html#a112dce5f001c5e7b3f006f078e89ce3c", null ],
    [ "Update", "class_player.html#a05b60cac1922c5be5c1be16baffa4497", null ],
    [ "sliding", "class_player.html#acab2124246368686ccf712aa52c99bdd", null ]
];