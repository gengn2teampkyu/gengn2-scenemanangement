var class_model_transform =
[
    [ "ModelTransform", "class_model_transform.html#a2a124eac7708c3caf9e6274fd0562b74", null ],
    [ "~ModelTransform", "class_model_transform.html#a7ba013c77b69b867aad0b02bd65398d9", null ],
    [ "getModel", "class_model_transform.html#a424b8a63791b89e8e3b9f81a553f04b1", null ],
    [ "GetPos", "class_model_transform.html#a4a341fd7144421d7e1027094a4f553ae", null ],
    [ "GetRot", "class_model_transform.html#ae97a63130d019d455975ba1e8b8e3559", null ],
    [ "GetScale", "class_model_transform.html#a688c62e1d75fc5f4775af8ca2b9915ba", null ],
    [ "SetPos", "class_model_transform.html#a487e8c5921bca377cef9627ea2477f86", null ],
    [ "SetRot", "class_model_transform.html#a09a40ee165ac7ba7b5f8ec7374901dd2", null ],
    [ "SetScale", "class_model_transform.html#a99c7f4df3aa7b3e00b802bab8ae2008a", null ]
];