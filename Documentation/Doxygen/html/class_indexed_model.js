var class_indexed_model =
[
    [ "CalcNormals", "class_indexed_model.html#ad7c6f8680a079108e64d463b34dca802", null ],
    [ "indices", "class_indexed_model.html#ae9ab23aa197180acd72e017503dd6a34", null ],
    [ "normals", "class_indexed_model.html#a43a9aa25e0461c1a729693fd7efaf45f", null ],
    [ "positions", "class_indexed_model.html#a81d6b9180bd152add38881ed6def521a", null ],
    [ "texCoords", "class_indexed_model.html#a8b7d3dd202865fb909f6cc07080f0f8d", null ]
];