var class_sound =
[
    [ "SoundType", "class_sound.html#ab83258fe5f67ebe1c8672cf18f5c965e", [
      [ "Music", "class_sound.html#ab83258fe5f67ebe1c8672cf18f5c965ea7fb661343007d3c8666af763f3cd2fa6", null ],
      [ "SFX", "class_sound.html#ab83258fe5f67ebe1c8672cf18f5c965eaa757f7662b4c49484df308f0aa15ebb2", null ]
    ] ],
    [ "Sound", "class_sound.html#a539c205cdf06fe2c621fd77c37bcfac9", null ],
    [ "~Sound", "class_sound.html#a0907389078bf740be2a5763366ad3376", null ],
    [ "InitializeSound", "class_sound.html#aaec730755fc01c9640deb0097aa70724", null ],
    [ "PausePlayback", "class_sound.html#a74324ca08bd6a8c77bf79b7caed201a4", null ],
    [ "PlayBGMusic", "class_sound.html#a5b83e0a4f72dccfa2c4179bff0ae54c9", null ],
    [ "PlaySFX", "class_sound.html#a89f242ea2c18e5421f4f7db3b09ce774", null ],
    [ "StopBGMPlayback", "class_sound.html#a27857d548b8115eb258a7a2d4b7bdcc9", null ]
];