var class_network_manager =
[
    [ "NetworkManager", "class_network_manager.html#ad64a0c0f05188bf6df49c0a7f4eede12", null ],
    [ "~NetworkManager", "class_network_manager.html#a2cfe4223139cf58587a9f066b956cb23", null ],
    [ "closeNetwork", "class_network_manager.html#a768e7f303b490ce05b09dc0ce5c4071a", null ],
    [ "getMessage", "class_network_manager.html#a0a0bba3f2eb61131bc940f667a939b8c", null ],
    [ "resetNetworkMessage", "class_network_manager.html#a8fde9da44f1554c4f07962f009bd2414", null ],
    [ "sendMessage", "class_network_manager.html#a44cb4cc617adbaa8d12213b4caecbc74", null ],
    [ "threadRun", "class_network_manager.html#a3e2f751d4e23120791bf6e002848f91c", null ],
    [ "hasMessageReceived", "class_network_manager.html#a7a55e826859c34a7930b883f8032e6bd", null ]
];