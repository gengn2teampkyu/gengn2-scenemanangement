var searchData=
[
  ['ikeypressed',['iKeyPressed',['../class_input_manager.html#ac5bb4fb344fd2875c0787136aa8ac0ba',1,'InputManager']]],
  ['init',['init',['../class_mesh.html#a7fd6927e45d33d55019a8ab935b055ab',1,'Mesh::init()'],['../class_shader.html#a3ea321aa0f441f9f8e0eea082ce748ec',1,'Shader::init()']]],
  ['initgameobject',['initGameObject',['../class_game_object.html#ae01c59954b61302d1e90a57e81c494f3',1,'GameObject']]],
  ['initializemesh',['initializeMesh',['../class_cube_renderer.html#a6d5ba0c46c99c16850d8e25537fd3c35',1,'CubeRenderer']]],
  ['initializesound',['initializeSound',['../class_audio_component.html#aa0a6fbf98a3d40dac7c92925f6f17c66',1,'AudioComponent']]],
  ['initializetext',['initializeText',['../class_text_renderer.html#a7a95db2e7da3485a0dec9be14aeef490',1,'TextRenderer']]],
  ['initmodel',['initModel',['../class_mesh.html#a817fa61ca01aeaae3251aca4bf1f3cb4',1,'Mesh']]],
  ['inittexturefile',['initTextureFile',['../class_texture.html#a6cd557615394dd8816e340ee61f917c0',1,'Texture']]],
  ['inputmanager',['InputManager',['../class_input_manager.html',1,'InputManager'],['../class_input_manager.html#a8be46886da639b26d67181c29dab6d6c',1,'InputManager::InputManager()']]],
  ['instantiateprefab',['InstantiatePrefab',['../class_prefab.html#a53b32b84802eabb87a00275b0093bfda',1,'Prefab::InstantiatePrefab()'],['../class_prefab.html#aefa9e71357ad50aa300dcbfd5920b73b',1,'Prefab::InstantiatePrefab(glm::vec3 position)']]],
  ['ismoving',['isMoving',['../class_player.html#a6a0b5131321ade6c0e9e30869507b587',1,'Player']]]
];
