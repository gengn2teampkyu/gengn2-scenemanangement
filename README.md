**PKYU Engine - A Component Based Design Engine**


**Engine Developers:**

**Aguilar, Paul**  - Engine Designer and Game Programmer
**Entuna, John Alson** - Engine and Component Based Design Programmer
**Orprecio, Justine** - Engine and Scene Management Programmer



**All Rights Reserved PKYU 2015**